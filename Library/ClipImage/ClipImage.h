//
//  ClipImage.h
//  iPost.order.mock
//
//  Created by 北川 義隆 on 2014/01/09.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ClipImage : NSObject

+ (UIImage *) resize:(UIImage *)original resize:(CGSize)resize;
@end
