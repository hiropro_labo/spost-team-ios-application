//
//  ClipImage.m
//  iPost.order.mock
//
//  Created by 北川 義隆 on 2014/01/09.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "ClipImage.h"

@implementation ClipImage

+ (UIImage *) resize:(UIImage *)original resize:(CGSize)resize
{
    
    // リサイズ画像のx,y,width,heightを算出
    float resized_x = 0.0;
    float resized_y = 0.0;
    float resized_width = resize.width;
    float resized_height = resize.height;
    
    // 縦横それぞれの「倍率」を算出し、より大きな倍率（＝大きな画像）を採用する。
    float ratio_width = resize.width/original.size.width;
    float ratio_height = resize.height/original.size.height;
    
    if( ratio_width < ratio_height ){
        // 縦の倍率採用
        resized_width = original.size.width*ratio_height;
        resized_x = (resize.width-resized_width)/2; // 横ははみ出る
    }else{
        // 横の倍率採用
        resized_height = original.size.height*ratio_width;
        resized_y = (resize.height-resized_height)/2; // 縦ははみ出る
    }
    
    // リサイズとクリップ処理
    CGSize resized_size = CGSizeMake(resize.width ,resize.height);
    UIGraphicsBeginImageContext(resized_size);
    
    // はみ出してる部分を切り落とす
    [original drawInRect:CGRectMake(resized_x, resized_y, resized_width, resized_height)];
    UIImage* resized_image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resized_image;
}

@end
