#!/usr/bin/env bash

convert $1 -geometry 29x29 Icon-Small.png
convert $1 -geometry 58x58 Icon-Small@2x.png

convert $1 -geometry 40x40 Icon-Small-40.png
convert $1 -geometry 80x80 Icon-Small-40pt@2x.png

convert $1 -geometry 50x50 Icon-Small-50.png
convert $1 -geometry 100x100 Icon-Small-50@2x.png

convert $1 -geometry 57x57 Icon.png
convert $1 -geometry 114x114 Icon@2x.png

convert $1 -geometry 120x120 Icon-60@2x.png

convert $1 -geometry 72x72 Icon-72.png
convert $1 -geometry 144x144 Icon72@2x.png

convert $1 -geometry 76x76 Icon-76.png
convert $1 -geometry 152x152 Icon-76@2x.png
