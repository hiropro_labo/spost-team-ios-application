//
//  AppDelegate.h
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2013/10/26.
//  Copyright (c) 2013年 北川 義隆. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
