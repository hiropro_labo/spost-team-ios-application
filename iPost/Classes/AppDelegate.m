//
//  AppDelegate.m
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2013/10/26.
//  Copyright (c) 2013年 北川 義隆. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // デバイストークンを要求
    if([[[UIDevice currentDevice] model] hasSuffix:@"Simulator"]){
        // サーバーに送信する
        NSString *tokenId = @"83d65fe153e1e063e08ccc78b2ca221f81c5a4717378ed255d46fea34d34f76b";
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithString:tokenId] forKey:DEF_TOKEN];
        NSLog(@"is Simulator Token");
        [self postDeviceToken:[NSString stringWithString:tokenId]];
    }else{
        [[UIApplication sharedApplication]
         registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
    /*
    if (application.applicationIconBadgeNumber > 0) {
        [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:DEF_TAB_INDEX];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else {
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:DEF_TAB_INDEX];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
*/
    // バッジを非表示にする
    application.applicationIconBadgeNumber = 0;
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - RemoteNotification

/*
 * APNsレスポンス（成功：デバイストークン取得）
 */
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)devToken
{
    NSMutableString *tokenId = [NSMutableString stringWithFormat:@"%@", devToken];
    [tokenId setString:[tokenId stringByReplacingOccurrencesOfString:@" " withString:@""]];
    [tokenId setString:[tokenId stringByReplacingOccurrencesOfString:@"<" withString:@""]];
    [tokenId setString:[tokenId stringByReplacingOccurrencesOfString:@">" withString:@""]];
    
    // サーバーに送信する
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithString:tokenId] forKey:DEF_TOKEN];
    [self postDeviceToken:[NSString stringWithString:tokenId]];
}

/*
 * APNsレスポンス（失敗）
 */
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)err
{
    TRACE(@"err = %@", err);
}

/*
 * デバイストークンをiPostのサーバーに送る
 */
- (void)postDeviceToken:(NSString *)devToken
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URL_API_TOKEN]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    
    NSString *data = [NSString stringWithFormat:@"client_id=%@&token=%@&device=1", CLIENT_ID, devToken];
    [request setHTTPBody:[data dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];
    if (connection) {
        TRACE(@"Token SEND : OK");
        TRACE(@"Client     : %@", CLIENT_ID);
        TRACE(@"Token      : %@", devToken);
    } else {
        TRACE(@"Token SEND : ERROR");
    }
}

#pragma mark -
/*
 * Push通知受取
 */
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    // アプリがフォアグラウンドで起動している時にPUSH通知を受信した場合
    if (application.applicationState == UIApplicationStateActive){
        TRACE(@"UIApplicationStateActive");
        application.applicationIconBadgeNumber = 0;
        NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
        
        //[self badgeView:userInfo];
    }
    
    // アプリがバックグラウンドで起動している時に、PUSH通知からアクティブになった場合
    if (application.applicationState == UIApplicationStateInactive){
        TRACE(@"UIApplicationStateInactive");
        application.applicationIconBadgeNumber = 0;
        NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
        
        //[self badgeView:userInfo];
    }
}

@end
