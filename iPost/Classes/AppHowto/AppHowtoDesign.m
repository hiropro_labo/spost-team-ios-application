//
//  AppHowtoDesign.m
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2014/01/01.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "AppHowtoDesign.h"
#import "IPMainModel.h"

@interface AppHowtoDesign () {
    NSMutableArray *_arrayDesign;
}
@end


@implementation AppHowtoDesign

- (NSInteger)numberOfDesign
{
    return [_arrayDesign count];
}


- (void)makeDesign
{
    for (int i = 0; i < SETTING_DESIGN_ROWTYPE_MAX; i++) {
        [self setDesign:[self makeDesignWithRowType:i]];
    }
}


/**
 * データ内容
 *  text      : タイトルテキスト
 *  accessory : accessoryのタイプ
 *  selection : selectionStyle
 *  url       : 遷移先のurl文字列
 */
- (NSDictionary *)makeDesignWithRowType:(settingRowType)rowType
{
    NSString *text;
    UITableViewCellAccessoryType accessoryType;
    UITableViewCellSelectionStyle selectionStyle;
    NSString *strUrl;
    IPMainModel *model;
    NSDictionary *mainJson;
    
    if ( ! model || model == nil) {
        model = [[IPMainModel alloc] init];
    }
    
    if ([model load:DEF_MAIN_JSON] == nil) {
        [model download:YES block:nil];
    }
    mainJson = [model load:DEF_MAIN_JSON];
    
    switch (rowType) {
        case SETTING_DESIGN_ROWTYPE_VERSION:
            text = [NSString stringWithFormat:@"バージョン　%@", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
            accessoryType = UITableViewCellAccessoryNone;
            selectionStyle = UITableViewCellSelectionStyleNone;
            strUrl = @"";
            break;
        case SETTING_DESIGN_ROWTYPE_USAGE:
            text = @"アプリの使い方";
            accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            selectionStyle = UITableViewCellSelectionStyleBlue;
            strUrl = URL_HP_HOWTO;
            break;
        case SETTING_DESIGN_ROWTYPE_FAQ:
            text = @"よくある質問";
            accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            selectionStyle = UITableViewCellSelectionStyleBlue;
            strUrl = URL_HP_FAQ;
            break;
            
        default:
            text = @"";
            accessoryType = UITableViewCellAccessoryNone;
            selectionStyle = UITableViewCellSelectionStyleNone;
            strUrl = @"";
            break;
    }
    
    NSDictionary *dic = @{SETTING_DESIGN_TEXT : text,
                          SETTING_DESIGN_ACCESSORY : [NSNumber numberWithInteger:accessoryType],
                          SETTING_DESIGN_SELECTIONSTYLE : [NSNumber numberWithInteger:selectionStyle],
                          SETTING_DESIGN_URL : strUrl
                          };
    
    return dic;
}

- (void)setDesign:(NSDictionary *)designData
{
    if (!_arrayDesign) {
        _arrayDesign = [[NSMutableArray alloc] init];
    }
    
    if (designData != nil) {
        [_arrayDesign addObject:designData];
    }
}

- (NSDictionary *)getDesignAtIndex:(NSInteger)index
{
    TRACE(@"index = %ld", (long)index);
    return _arrayDesign[index];
}

@end
