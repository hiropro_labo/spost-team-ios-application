//
//  AppHowtoViewController.m
//  iPost.mock.v3
//
//  Created by 北川 義隆 on 2014/04/15.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "AppHowtoViewController.h"
#import "SettingCustomCell.h"
#import "ApphowtoDesign.h"
#import "ReportViewController.h"

#define SETTING_CUSTOMCELL @"settingCustomCell"

@interface AppHowtoViewController () {
    AppHowtoDesign *_designData;
}

@end

@implementation AppHowtoViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    TRACE(@"");
    
    self.navigationItem.title = NAVIGATION_TITLE;
    
    if (!_designData || _designData == nil) {
        _designData = [[AppHowtoDesign alloc] init];
        TRACE(@"_designData = %@", _designData);
    }
    [_designData makeDesign];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"SettingCustomCell" bundle:nil] forCellReuseIdentifier:SETTING_CUSTOMCELL];
    
    UIBarButtonItem *backBarButtonItem =
    [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:BACK_BUTTON_IMG]
                                     style:UIBarButtonItemStylePlain
                                    target:self.navigationController
     
                                    action:@selector(popViewControllerAnimated:)];
    self.navigationItem.leftBarButtonItem = backBarButtonItem;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_designData numberOfDesign];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section;
{
    NSString *resString;
    switch (section) {
        case 1:
            resString = @"ヘルプ";
            break;
        case 3:
            resString = @"　";
            break;
        default:
            resString = nil;
            break;
    }
    
    return resString;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return nil;
}


- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section;
{
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TRACE(@"");
    SettingCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:SETTING_CUSTOMCELL forIndexPath:indexPath];
    
    [cell putEffect];
    
    NSDictionary *dic = [_designData getDesignAtIndex:indexPath.section];
    
    cell.accessoryType = [dic[SETTING_DESIGN_ACCESSORY] intValue];
    cell.selectionStyle = [dic[SETTING_DESIGN_SELECTIONSTYLE] intValue];
    cell.switchInput.hidden = YES;
    
    cell.labelTitle.text = dic[SETTING_DESIGN_TEXT];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = [_designData getDesignAtIndex:indexPath.section];
    
    // Webビュー
    if (indexPath.section == SETTING_DESIGN_ROWTYPE_USAGE
        || indexPath.section == SETTING_DESIGN_ROWTYPE_FAQ) {
        // 「戻る」に名称変更
        UIBarButtonItem *back_button = [[UIBarButtonItem alloc] initWithTitle:@"戻る" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.navigationItem.backBarButtonItem = back_button;
        
        TRACE(@"strUrl = %@", dic[SETTING_DESIGN_URL]);
        [self performSegueWithIdentifier:@"settingHelpWebview" sender:self];
    }
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
}


#pragma mark - public method

- (NSString *)getUrl
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    NSDictionary *dic = [_designData getDesignAtIndex:indexPath.section];
    
    return dic[SETTING_DESIGN_URL];
}

@end
