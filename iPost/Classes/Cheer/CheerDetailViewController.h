//
//  MenuDetailViewController.h
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2013/10/28.
//  Copyright (c) 2013年 北川 義隆. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IPCheerModel.h"

#define COLOR_DETAIL     RGB(50, 50, 50)
#define DETAIL_FONTSIZE  13.
#define DETAIL_WIDTH     280.

@interface CheerDetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *birthDayLabel;
@property (weak, nonatomic) IBOutlet UILabel *hobbyLabel;
@property (weak, nonatomic) IBOutlet UILabel *abilityLabel;
@property (weak, nonatomic) IBOutlet UILabel *homeTownLabel;



- (void)setData:(NSDictionary *)dicData;
- (NSDictionary *)getData;
- (NSURL *)getUrl;
- (void)setUrl:(NSURL *)str;
@end
