//
//  MenuDetailViewController.m
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2013/10/28.
//  Copyright (c) 2013年 北川 義隆. All rights reserved.
//

#import "CheerDetailViewController.h"
#import "AFNetworking.h"

@interface CheerDetailViewController () <OHAttributedLabelDelegate>
{
    IBOutlet UIScrollView *_scrollView;
    IBOutlet UIImageView *_imageView; // 上部画像用
    
    IPCheerModel *_model;
    NSDictionary *_menu;
    
    NSURL *_url;
}
@end


@implementation CheerDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {}
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = NAVIGATION_TITLE;
    
    _menu = [_model getDataAtIndex:0];
    
    UIBarButtonItem *backBarButtonItem =
    [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:BACK_BUTTON_IMG]
                                     style:UIBarButtonItemStylePlain
                                    target:self.navigationController
     
                                    action:@selector(popViewControllerAnimated:)];
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    TRACE(@"");
    
    /// 選手画像
    NSString *strURL = [NSString stringWithFormat:@"%@%@",
                        URL_IMG_CHEER_MEMBER,
                        [_menu objectForKey:JSON_KEY_CHEER_LIST_FILE]];
    NSURL *urlImage = [NSURL URLWithString:strURL];
    TRACE(@"urlImage = %@", urlImage);

    [_imageView setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:IMAGE_PLACEHOLDERIMAGE_MENU_DETAIL]];
    TRACE(@"image w = %f, h = %f",_imageView.image.size.width, _imageView.image.size.height);
    
    //誕生日
    self.birthDayLabel.text = [_menu objectForKey:JSON_KEY_CHEER_LIST_BIRTHDAY];
    
    //趣味
    self.hobbyLabel.text = [_menu objectForKey:JSON_KEY_CHEER_LIST_HOBBY];
    
    //特技
    self.abilityLabel.text = [_menu objectForKey:JSON_KEY_CHEER_LIST_ABILITY];
    
    //出身地
    self.homeTownLabel.text = [_menu objectForKey:JSON_KEY_CHEER_LIST_HOMETOWN];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    TRACE(@"");
    
    CGSize size = _scrollView.contentSize;
    size.height = 405;
    _scrollView.contentSize = size;
    
}

/**
 * 全角判断
 * @return bool YES:半角/NO:全角含む
 */
- (BOOL)isAllHalfWidthCharacter:(NSString *)num
{
    NSUInteger nsStringlen = [num length];
    const char *utf8 = [num UTF8String];
    size_t cStringlen = strlen(utf8);
    if (nsStringlen == cStringlen) {
        return YES;
    } else {
        return NO;
    }
}


#pragma mark - public method

- (void)setData:(NSDictionary *)dicData
{
    if (!_model || _model == nil) {
        _model = [[IPCheerModel alloc] init];
    }
    [_model setData:dicData];
}

- (NSDictionary *)getData
{
    NSDictionary *dic = [_model getDataAtIndex:0];
    
    return dic;
}

- (NSURL *)getUrl
{
    return _url;
}

#pragma mark - private method
- (void)setUrl:(NSURL *)str
{
    _url = str;
}


#pragma mark - OHAttributedLabel Delegate Method

-(BOOL)attributedLabel:(OHAttributedLabel *)attributedLabel shouldFollowLink:(NSTextCheckingResult *)linkInfo
{
    if ([[UIApplication sharedApplication] canOpenURL:linkInfo.extendedURL])
    {
        [self setUrl:linkInfo.extendedURL];
        [self performSegueWithIdentifier:@"menuWebview" sender:self];
        return NO;
    }
    
    return YES;
}

@end
