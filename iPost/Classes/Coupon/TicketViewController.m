//
//  CouponViewController.m
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2013/10/26.
//  Copyright (c) 2013年 北川 義隆. All rights reserved.
//

#import "TicketViewController.h"

@interface TicketViewController ()
{
}

@end


@implementation TicketViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {}
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    TRACE(@"");
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    TRACE(@"");
    
    //ScrollViewスクロールサイズ(height)
    CGSize size = _scrollView.contentSize;
    size.height = 2470;
    _scrollView.contentSize = size;
    
    [self.view layoutSubviews];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
