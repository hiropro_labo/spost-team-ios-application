//
//  Carousel.h
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2013/12/30.
//  Copyright (c) 2013年 北川 義隆. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainCarousel : UIView <UIScrollViewDelegate, PageControlDelegate>
{
    UIScrollView *_scrollView;
    PageControl *_pageControl;
    NSArray *_images;
    
    BOOL _circulated;
    BOOL _autoScrollStopped;
    BOOL _autoBackScrollStopped;
    
    NSTimer *_timer;
    NSTimer *_backTimer;
}

@property (nonatomic, retain) UIScrollView *_scrollView;
@property (nonatomic, retain) PageControl *_pageControl;
@property (nonatomic, retain) NSArray *_images;
@property (nonatomic, retain) NSTimer *_timer;
@property (nonatomic, retain) NSTimer *_backTimer;
@property (nonatomic, readonly, getter=isCirculated) BOOL _circulated;

- (void)setImages:(NSArray *)newImages;
- (void)setup;
- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
- (void)timerStop;
@end