//
//  Carousel.m
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2013/12/30.
//  Copyright (c) 2013年 北川 義隆. All rights reserved.
//

#import "MainCarousel.h"

#define IMAGE_WIDTH          320
#define	AUTO_SCROLL_DELAY    2
#define SCROLL_INTERVAL      0.05
#define SCROLL_STOP_INTERVAL 3.
#define SCROLL_BACK_INTERVAL 0.005

@interface MainCarousel () {
    int _scrollCnt;
    NSTimer *_interval;
}

@end
@implementation MainCarousel

@synthesize _scrollView;
@synthesize _pageControl;
@synthesize _images;
@synthesize _timer;
@synthesize _backTimer;
@synthesize _circulated;


#pragma mark - Override images setter

- (void)setImages:(NSArray *)newImages
{
    if (newImages != _images)
    {
        _images = newImages;
        
        [self setup];
    }
}


#pragma mark - Carousel setup

- (void)setup
{
    AFImageRequestOperation *operation;
    NSOperationQueue *queue;
    NSURL *url;
    NSString *strURL;
    
    // スクロールビューの準備
    CGSize size   = self.frame.size;
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    CGRect imgRect = CGRectMake(0, 0, size.width, size.height);
    _scrollView = [[UIScrollView alloc] initWithFrame:rect];
    [_scrollView setDelegate:self];
    [_scrollView setShowsHorizontalScrollIndicator:NO];
    [_scrollView setShowsVerticalScrollIndicator:NO];
    [_scrollView setPagingEnabled:YES];
    [_scrollView setBounces:NO];

    CGSize scrollViewSize = _scrollView.frame.size;
    
    // 画像分ダウンロード＆スクロールビューへセット
    int imgCnt = (int)[_images count];
    for (NSInteger i = 0; i < imgCnt; i++)
    {
        CGRect slideRect = CGRectMake(scrollViewSize.width * i, 0, size.width, size.height);
        
        //画像か動画か
        int fileType = [[[_images objectAtIndex:i] objectForKey:@"type"] intValue];
        
        if(fileType == 1){
            //動画URL
            NSString *moviewURL = [[_images objectAtIndex:i] objectForKey:@"movie_url"];

            //YouTubeのタグ
            NSString *htmlSrc = [NSString stringWithFormat:@"<html><body><object width=\"305\" height=\"175\"><param name=\"movie\" value=\"http://www.youtube.com/v/%@\"></param><param name=\"wmode\" value=\"transparent\"></param><embed src=\"http://www.youtube.com/v/%@\" type=\"application/x-shockwave-flash\" wmode=\"transparent\" width=\"305\" height=\"175\"></embed></object></body></html>", moviewURL, moviewURL];

            
            UIWebView *webView = [[UIWebView alloc] initWithFrame:slideRect];
            [webView loadRequest:nil];
            webView.scrollView.scrollEnabled = NO;
            [webView loadHTMLString:htmlSrc baseURL: nil];
            [_scrollView addSubview:webView];
        }
        else{

            strURL = [NSString stringWithFormat:@"%@%@",
                  URL_IMG_MAIN,
                  [[_images objectAtIndex:i] objectForKey:@"file_name"]];
        
            TRACE(@"%@", strURL);
        
            url = [NSURL URLWithString:strURL];
        
            UIView *slide = [[UIView alloc] initWithFrame:slideRect];
            [slide setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0]];
        
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:imgRect];
        
            operation = [AFImageRequestOperation
                     imageRequestOperationWithRequest:[NSURLRequest requestWithURL:url]
                     imageProcessingBlock:nil
                     success:^(NSURLRequest *request, NSHTTPURLResponse *response, id IMG){
                         [imageView setImage:IMG];
                     } failure:nil];
            queue = [[NSOperationQueue alloc] init];
            [queue addOperation:operation];

            [slide addSubview:imageView];
        
            [_scrollView addSubview:slide];
        }
    }
    
    // ページコントロールの設定
    _pageControl = [[PageControl alloc] initWithFrame:CGRectMake(0, scrollViewSize.height - 20, scrollViewSize.width, 20)];
    _pageControl.numberOfPages = [_images count];
    _pageControl.currentPage = 0;
    _pageControl.delegate = self;
    
    [_scrollView setContentSize:CGSizeMake(scrollViewSize.width * [_images count], scrollViewSize.height)];
    
    [self addSubview:_scrollView];
    [self addSubview:_pageControl];
    
    if ([_images count] > 1) {
        //_circulated = YES;
    }
    
    _autoScrollStopped = NO;
    _autoBackScrollStopped = NO;
    _scrollCnt = 1;
    
	if (_circulated) {
		[self stopAutoScroll];
        
		if ([_timer isValid]) {
			[_timer invalidate];
		}
		_timer = [NSTimer scheduledTimerWithTimeInterval:SCROLL_INTERVAL
                                                  target:self
                                                selector:@selector(timerDidFire:)
                                                userInfo:nil
                                                 repeats:YES];
		[self restartAutoScrollAfterDelay];
	}
}

- (void)setUpWebView:(NSArray *)arrayData
{
    
}

#pragma mark - Timer

- (void)timerStop
{
    _autoScrollStopped = YES;
    _autoBackScrollStopped = YES;
    [_timer invalidate];
	_timer = nil;
    [_backTimer invalidate];
    _backTimer = nil;
}

- (void)timerDidFire:(NSTimer *)timer
{
    if (_autoScrollStopped) {
        return;
    }
    
    CGPoint p = _scrollView.contentOffset;
    if ( ! _autoBackScrollStopped) {
        p.x = p.x + 10;
    }
    
    if (p.x < (IMAGE_WIDTH * ([_images count])) && ! _autoBackScrollStopped) {
        if (p.x == IMAGE_WIDTH * _scrollCnt) {
            _autoScrollStopped = YES;
            _interval = [NSTimer scheduledTimerWithTimeInterval:SCROLL_STOP_INTERVAL
                                                         target:self
                                                       selector:@selector(timerInterval:)
                                                       userInfo:nil
                                                        repeats:NO];
        }
        _scrollView.contentOffset = p;
    }
}

- (void)timerInterval:(NSTimer *)timer
{
    if (_scrollCnt < ([_images count] - 1)) {
        _scrollCnt ++;
    } else {
        _autoBackScrollStopped = YES;
        
        _autoScrollStopped = YES;
		_backTimer = [NSTimer scheduledTimerWithTimeInterval:SCROLL_BACK_INTERVAL
                                                      target:self
                                                    selector:@selector(timerBackScroll:)
                                                    userInfo:nil
                                                     repeats:YES];
    }
    
    _autoScrollStopped = NO;
}


- (void)timerBackScroll:(NSTimer *)timer
{
    if ( ! _autoBackScrollStopped) {
        return;
    }
    
    CGPoint p = _scrollView.contentOffset;
    p.x = p.x - 10;
    _scrollView.contentOffset = p;
    
    if (p.x == 0) {
        _scrollCnt = 0;
        
        [_backTimer invalidate];
        _backTimer = nil;
        
        _interval = [NSTimer scheduledTimerWithTimeInterval:SCROLL_STOP_INTERVAL
                                                     target:self
                                                   selector:@selector(timerIntervalBack:)
                                                   userInfo:nil
                                                    repeats:NO];
    }
}


- (void)timerIntervalBack:(NSTimer *)timer
{
    _autoBackScrollStopped = NO;
    _scrollCnt = 1;
}


- (void)restartAutoScrollAfterDelay
{
    [self performSelector:@selector(restartAutoScroll)
               withObject:nil
               afterDelay:AUTO_SCROLL_DELAY];
}

- (void)restartAutoScroll
{
    _autoScrollStopped = NO;
}

- (void)stopAutoScroll
{
    _autoScrollStopped = YES;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self stopAutoScroll];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self restartAutoScrollAfterDelay];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate) {
        [self restartAutoScrollAfterDelay];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGPoint origin = [scrollView contentOffset];
    [scrollView setContentOffset:CGPointMake(origin.x, 0.0)];
    
	CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
	[_pageControl setCurrentPage:page];
}

@end
