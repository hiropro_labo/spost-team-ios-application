//
//  MainViewController.h
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2013/10/26.
//  Copyright (c) 2013年 北川 義隆. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IPMainModel.h"
#import "IPMainMenuModel.h"
#import "IPScheduleModel.h"

@interface MainViewController : UIViewController<UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *carouselView;

@property (weak, nonatomic) IBOutlet UIWebView *mainWebCarousel;

@property (strong, nonatomic) UITableView *tableView;

// Button View
@property (strong, nonatomic) IBOutlet UIView *btnLeftView;
@property (strong, nonatomic) IBOutlet UIView *btnCenterView;
@property (strong, nonatomic) IBOutlet UIView *btnRightView;

// Button Button
@property (strong, nonatomic) IBOutlet UIButton *btnLeftButton;
@property (strong, nonatomic) IBOutlet UIButton *btnCenterButton;
@property (strong, nonatomic) IBOutlet UIButton *btnRightButton;
@property (weak, nonatomic) IBOutlet UIView *sponsorButtonView;
@property (weak, nonatomic) IBOutlet UIButton *sponsorButton;

- (NSDictionary *)getCurrData;
- (NSDictionary *)getClientInfo;

@end
