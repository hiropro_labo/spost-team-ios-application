//
//  MainViewController.m
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2013/10/26.
//  Copyright (c) 2013年 北川 義隆. All rights reserved.
//

#import "MainViewController.h"
#import "MainCarousel.h"
#import "IPNewsModel.h"
#import "WebViewController.h"
#import "NewsDetailViewController.h"
#import "ScheduleDetailViewcontroller.h"
#import "SponsorViewController.h"

#define CORNERRADIUS 2.
#define SHADOWWIDTH 1.
#define SHADOWHEIGHT 1.


@interface MainViewController ()
{
    IPMainModel   *_model;
    NSDictionary *_mainJson;
    UIColor *_color;
    MainCarousel *_carousel;
    UIScrollView *scrollView;
    
    //news
    NSMutableArray *newsArray;
    IPNewsModel   *news_model;
    NSDictionary *_newsJson;
    NSURL *_url;

    //socialMenu
    IPMainMenuModel *_menuModel;
    NSDictionary *socialListArray;
    NSArray *socialKeys;
    
    //スケジュール
    IPScheduleModel *_scheduleModel;
    NSArray *scheduleArray;
    
//    NSString *_parent; // 親データのカテゴリID
//    NSUInteger *_index; // テーブルID
    
}

@end


@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {}
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    TRACE(@"");
    
    //self.navigationItem.title = NAVIGATION_TITLE;
    
    if ( ! _model || _model == nil) {
        _model = [[IPMainModel alloc] init];
    }
    
    if ([_model load:DEF_MAIN_JSON] == nil) {
        [_model download:YES block:nil];
    }
    
    _mainJson = [_model load:DEF_MAIN_JSON];
    
    // News Model の読み込み
    if ( ! news_model || news_model == nil) {
        news_model = [[IPNewsModel alloc] init];
    }
    
    //スライドメニュー読み込み
    if ( ! _menuModel || _menuModel == nil) {
        _menuModel = [[IPMainMenuModel alloc] init];
    }
    
    if ([_menuModel load:DEF_MAINMENU_JSON] == nil) {
        [_menuModel download:YES block:nil];
    }
    /*
    // スケジュール Model の読み込み
    if ( ! _scheduleModel || _scheduleModel == nil) {
        _scheduleModel = [[IPScheduleModel alloc] init];
    }
    
    [_scheduleModel download:YES block:nil];
*/
    [self.mainWebCarousel loadRequest:nil];
    self.mainWebCarousel.delegate = self;
    self.mainWebCarousel.scalesPageToFit = YES;
    [self setupView];
    
    //[self setupCarousel];
    
    [self newsTableSetup];
    
    [self setupButtons];
    
    [self setupScrollView];

    //スポンサーアプリ一覧ボタン隠す
    NSDictionary *_menuJson = [_menuModel loadListMenu];
    //NSMutableArray *menuArray = [[NSMutableArray alloc] init];

    for (NSDictionary *obj in _menuJson) {
        if (obj != nil)
        {
            if(![[obj objectForKey:JSON_KEY_MENU_CONTROLLER] isEqualToString:@"sponsor"]){
                //self.sponsorButton.hidden = YES;
            }
        }
    }

    //UIScrollViewを取得後、bouncesをNOに変更
    for (id subview in self.mainWebCarousel.subviews){
        if([[subview class] isSubclassOfClass: [UIScrollView class]]){
            ((UIScrollView *)subview).bounces = NO;
        }
    }
    
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
#warning パラメーターで詳細ページのデータを渡す
    //scheme:appで画面遷移
    if([[[request URL] scheme] isEqualToString:@"app"]){
        
        scheduleArray = [[NSMutableArray array] init];
        scheduleArray = [_scheduleModel loadList];
        
        ScheduleDetailViewController *scheduleDetilView = [self.storyboard instantiateViewControllerWithIdentifier:@"scheduleDetail"];
        [scheduleDetilView setData:[scheduleArray objectAtIndex:1]];
        [self.navigationController pushViewController:scheduleDetilView animated:YES];

        return NO;
    }
    
    return YES;
}

/**
 * 画面が表示される直前の処理
 */
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    TRACE(@"");
    
    if ([news_model load:DEF_NEWS_JSON] == nil) {
        [news_model download:YES block:nil];
    }
    
    [self jsonLoad];
    
    [self setupCarousel];

    [self.tableView reloadData];
    
    NSURL *url = [NSURL URLWithString:URL_APP_TOP_CAROUSEL];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    [self.mainWebCarousel loadRequest:req];
}

/**
 * 画面の表示が終わる直前の処理
 */
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_carousel timerStop];
    
    //self.mainWebCarousel.delegate = nil;
    //[self.mainWebCarousel removeFromSuperview];
    //self.mainWebCarousel = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];

}

/**
 * ストーリーボード用背景の透明化など
 */
- (void)setupView
{
    // ビューの背景は白色
    [self.carouselView setBackgroundColor:[UIColor whiteColor]];
    
    // scrollView
    CGRect m_rcMainSrcn = [[UIScreen mainScreen] applicationFrame];
    CGFloat width = m_rcMainSrcn.size.width;
    CGFloat height = m_rcMainSrcn.size.height;
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 49, width, height)];
    scrollView.delegate = self;
    scrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    scrollView.scrollEnabled = YES;
    scrollView.bounces = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:scrollView];
}


/**
 * カルーセル
 */
- (void)setupCarousel
{

    // Carousel Position and Size
    CGSize size   = self.carouselView.frame.size;
    CGRect rect   = CGRectMake(0, 0, size.width, size.height);

    // Initialize carousel object with frame size of images
    _carousel = [[MainCarousel alloc] initWithFrame:rect];
    
    // Add some images to carousel, we are passing autoreleased NSArray
    [_carousel setImages:[_mainJson objectForKey:JSON_KEY_MAIN_IMAGE]];
    
    // Add carousel to view
    [self.carouselView addSubview:_carousel];
    
    // Add scrollView to scrollView
    [scrollView addSubview:self.carouselView];
    
    //[scrollView addSubview:webview];
    [scrollView addSubview:self.mainWebCarousel];
}

- (void)newsTableSetup
{
    // News Table
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
//    CGFloat tableOrignY = newsBar.frame.origin.y + newsBar.frame.size.height;
    self.tableView.frame = CGRectMake(0, 0, 320, 104);
    //self.tableView.backgroundColor = [UIColor blackColor];
    UIImage *backgroundImage = [UIImage imageNamed:@"news_background.png"];
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:backgroundImage];
    self.tableView.backgroundColor = RGBA(0, 0, 0, 0.99f);
    self.tableView.bounces = NO;
    
    // Newsリストのセパレータを画像に
    UIImage *newsDotLine = [UIImage imageNamed:@"news_line_dot.png"];
    UIColor *color = [UIColor colorWithPatternImage:newsDotLine];
    [self.tableView setSeparatorColor:color];
}

#warning 要push通知でリロードのテスト
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    // アプリがフォアグラウンドで起動している時にPUSH通知を受信した場合
    if (application.applicationState == UIApplicationStateActive){
        TRACE(@"UIApplicationStateActive");
        application.applicationIconBadgeNumber = 0;
        NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
        
        [self.tableView reloadData];
    }
}

/**
 * ボタンセットアップ
 */
- (void)setupButtons
{
    [self setupBtnShadow:self.btnLeftView];
    [self setupBtnShadow:self.btnCenterView];
    [self setupBtnShadow:self.btnRightView];
    
    self.btnLeftView.frame = CGRectMake(
                                        self.btnLeftView.frame.origin.x,
                                        self.carouselView.frame.size.height,
                                        self.btnLeftView.frame.size.width,
                                        self.btnLeftView.frame.size.height);

    self.btnCenterView.frame = CGRectMake(
                                        self.btnCenterView.frame.origin.x,
                                        self.carouselView.frame.size.height,
                                        self.btnCenterView.frame.size.width,
                                        self.btnCenterView.frame.size.height);

    self.btnRightView.frame = CGRectMake(
                                        self.btnRightView.frame.origin.x,
                                        self.carouselView.frame.size.height,
                                        self.btnRightView.frame.size.width,
                                        self.btnRightView.frame.size.height);

    //Add buttons to scrollView
    [scrollView addSubview:self.btnLeftView];
    [scrollView addSubview:self.btnCenterView];
    [scrollView addSubview:self.btnRightView];
}

- (void)setupBtnShadow:(UIView *)view
{
    [view setBackgroundColor:[UIColor colorWithRed:255/255 green:255/255 blue:255/255 alpha:.9]];
    view.layer.cornerRadius = CORNERRADIUS;
    
    view.layer.shadowOpacity = 0.1;
    view.layer.shadowRadius = 1.5;
    view.layer.shadowOffset = CGSizeMake(SHADOWWIDTH, SHADOWHEIGHT);
    view.layer.shadowPath = [UIBezierPath bezierPathWithRect:view.bounds].CGPath;
    view.layer.shouldRasterize = YES;
    view.layer.rasterizationScale = [UIScreen mainScreen].scale;
}

/*
 scrollView 設定
 */
- (void)setupScrollView
{
    // News Bar
    CGFloat barOrignY = self.btnLeftView.frame.origin.y + self.btnLeftView.frame.size.height;
    
    //webCarousel
    self.mainWebCarousel.frame = CGRectMake(0, barOrignY, self.mainWebCarousel.frame.size.width, self.mainWebCarousel.frame.size.height);
    
    CGFloat webCarouselY = self.mainWebCarousel.frame.origin.y + self.mainWebCarousel.frame.size.height;
    UIImageView *newsBar = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"news_bar.png"]];
    newsBar.frame = CGRectMake(0, webCarouselY, newsBar.frame.size.width, newsBar.frame.size.height);
    
    // newsテーブル
    CGFloat tableOrignY = newsBar.frame.origin.y + newsBar.frame.size.height;
    self.tableView.frame = CGRectMake(0, tableOrignY, 320, 104);
    
    // Sponsor Bar
    CGFloat sponsorBarOrignY = self.tableView.frame.origin.y + self.tableView.frame.size.height;
    UIImageView *sponsorBar = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sponsor_bar.png"]];
    sponsorBar.frame = CGRectMake(0, sponsorBarOrignY, sponsorBar.frame.size.width, sponsorBar.frame.size.height);
    
    // Sponsor Image
    CGFloat sponsorImageOrignY = sponsorBar.frame.origin.y + sponsorBar.frame.size.height;
    UIImageView *sponsorImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sponsor.png"]];
    sponsorImageView.frame = CGRectMake(0, sponsorImageOrignY + 40, sponsorImageView.frame.size.width, sponsorImageView.frame.size.height);
    
    // スポンサー一覧ボタン
    self.sponsorButtonView.frame = CGRectMake(0, sponsorImageOrignY, self.sponsorButtonView.frame.size.width, self.sponsorButtonView.frame.size.height);
    
    // Footer
    CGFloat footerOrignY = sponsorImageView.frame.origin.y + sponsorImageView.frame.size.height;
    UIImageView *footerImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"footer.png"]];
    footerImageView.frame = CGRectMake(0, footerOrignY, footerImageView.frame.size.width, footerImageView.frame.size.height);
    
    
    [scrollView addSubview:newsBar];
    
    [scrollView addSubview:self.tableView];
    
    [scrollView addSubview:sponsorBar];
    
    [scrollView addSubview:sponsorImageView];
    
    [scrollView addSubview:footerImageView];
    
    [scrollView addSubview:self.sponsorButtonView];
    
    [self setUpSocialButtons:footerOrignY];
    
    CGFloat scrollViewContentHeight = (footerImageView.frame.origin.y + 93) + footerImageView.frame.size.height;
    scrollView.contentSize = CGSizeMake(320, scrollViewContentHeight);
}

- (void)setUpSocialButtons:(CGFloat)footerOrignY
{
    socialListArray = [[NSDictionary alloc] init];
    socialListArray = [_menuModel loadListSocial];
    
    if(socialListArray.count > 0){
        socialKeys = [socialListArray allKeys];
        int i = 0;
        for (id key in socialKeys) {
            
            UIButton *socialButton =
            [UIButton buttonWithType:UIButtonTypeRoundedRect];
            socialButton.tag = i;
            
            i++;
            socialButton.frame = CGRectMake(320 - (50 * i), footerOrignY + 8, 30, 30);
            
            [socialButton addTarget:self action:@selector(socialButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            
            if([key isEqualToString:@"twitter"]){
                [socialButton setBackgroundImage:[UIImage imageNamed:@"icon_twitter.png"] forState:UIControlStateNormal];
            }
            else if([key isEqualToString:@"facebook"]){
                [socialButton setBackgroundImage:[UIImage imageNamed:@"icon_facebook.png"] forState:UIControlStateNormal];
            }
            else if([key isEqualToString:@"youtube"]){
                [socialButton setBackgroundImage:[UIImage imageNamed:@"icon_youtube.png"] forState:UIControlStateNormal];
            }
            
            [scrollView addSubview:socialButton];
            
        }
    }
}

- (void)socialButtonAction:(UIButton *)sender
{
    NSString *urlString = [socialListArray objectForKey:[socialKeys objectAtIndex:sender.tag]];
    
    NSURL* url = [[NSURL alloc] initWithString:urlString];
    
    [[UIApplication sharedApplication] openURL:url];
    
}
- (IBAction)sponsorButtonAction:(id)sender
{
    SponsorViewController *sponsorView = [self.storyboard instantiateViewControllerWithIdentifier:@"sponsor"];
    
    UIBarButtonItem *backBarButtonItem =
    [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:BACK_BUTTON_IMG]
                                     style:UIBarButtonItemStylePlain
                                    target:self.navigationController
     
                                    action:@selector(popViewControllerAnimated:)];
    sponsorView.navigationItem.leftBarButtonItem = backBarButtonItem;
    
    [self.navigationController pushViewController:sponsorView animated:YES];
}

/**
 * セルの数
 */
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    NSInteger cnt = [newsArray count];
    if (cnt > 0) {
        return cnt;
    } else {
        return 1;
    }
}

/**
 * テーブルのセル表示処理
 */

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    cell.backgroundColor = [UIColor yellowColor];
    
    NSString *title = [[newsArray objectAtIndex:indexPath.row] objectForKey:JSON_KEY_NEWS_TITLE];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font = [UIFont boldSystemFontOfSize:11.f];
    cell.textLabel.text = title;
    
    //アクセサリー画像
    UIImage *arrowImage = [UIImage imageNamed:@"arrow_yellow.png"];
    UIImageView *arrowImageView = [[UIImageView alloc] initWithImage:arrowImage];
    
    //画像が大きい場合にはみ出さないようにViewの大きさを固定
    arrowImageView.frame = CGRectMake(0, 0, 10, 15);
    //アクセサリービューにイメージを設定
    cell.accessoryView = arrowImageView;
    
    //ニュースがなかったら
    if([[[newsArray objectAtIndex:indexPath.row] objectForKey:@"empty_check"] isEqualToString:@"1"]){
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryView = nil;
    }
    return cell;
}

//ニュースが空の時は遷移を無効にする
-(NSIndexPath *)tableView:(UITableView *)tableView
 willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[[newsArray objectAtIndex:0] objectForKey:@"empty_check"] isEqualToString:@"1"])
        return nil;
    else
        return indexPath;
}

/**
 * テーブルのセルTap時
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TRACE(@"TABLE CEL SELECT : %ld", (long)indexPath.row);

    [self performSegueWithIdentifier:@"newsFromMainSegue" sender:self];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

/**
 * テーブルの背景色
 */
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
}

/**
 * テーブルのセルの高さ
 */
- (CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
    return 26;
}

- (void)jsonLoad
{
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = @"Loading...";
    
    [hud showAnimated:YES whileExecutingBlock:^{
        _newsJson = [news_model load:DEF_NEWS_JSON];
        newsArray = [[NSMutableArray alloc] init];
        
        if(_newsJson.count == 0){
            NSMutableDictionary *emptyDictionary = [[NSMutableDictionary alloc] init];
            [emptyDictionary setObject:@"まだニュースはありません" forKey:JSON_KEY_NEWS_TITLE];
            [emptyDictionary setObject:@"1" forKey:@"empty_check"];
            [newsArray addObject:emptyDictionary];
        }
        else{
            for (NSDictionary *obj in _newsJson) {
                if (obj != nil)
                {
                    [newsArray addObject:obj];
                }
            }
        }
        
        [self.tableView reloadData];
        
    } completionBlock:^{
        [hud removeFromSuperview];
    }];
}

#pragma mark - public method
/*
- (void)setParent:(NSString *)parent
{
    _parent = parent;
}

- (void)setIndex:(NSUInteger *)index
{
    _index = index;
}
*/
- (NSDictionary *)getCurrData
{
    return [newsArray objectAtIndex:[_tableView indexPathForSelectedRow].row];
}

- (NSDictionary *)getClientInfo
{
    NSDictionary *dic = [_mainJson objectForKey:JSON_KEY_MAIN_SHOP];
    return dic;
}

@end
