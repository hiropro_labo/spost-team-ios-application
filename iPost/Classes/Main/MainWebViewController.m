//
//  MainWebViewController.m
//  iPost.mock.v4
//
//  Created by ヒロ企画 on 2014/04/23.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "MainWebViewController.h"

@interface MainWebViewController ()
{
    NSURLRequest *URLreq;
    NSDictionary *webViewData;
}
@end

@implementation MainWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.webView.delegate = self;
    
    //ページをwebViewのサイズに合わせて表示
    //self.webView.scalesPageToFit = YES;
    //self.navigationController.navigationBar.barTintColor =
    //[UIColor colorWithRed:0.077 green:0.411 blue:0.672 alpha:0.200];
    
    //UIScrollViewを取得後、bouncesをNOに変更
    for (id subview in self.webView.subviews){
        if([[subview class] isSubclassOfClass: [UIScrollView class]]){
            ((UIScrollView *)subview).bounces = NO;
        }
    }
    
    //wevViewDataの取得
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    webViewData = [[NSDictionary alloc] init];
    webViewData = [ud objectForKey:@"webViewData"];

    //取得した後残ると嫌なのですぐに削除
    [ud removeObjectForKey:@"webViewData"];

    //wevViewDataを元にURLrequest
    NSString *webViewURL = [NSString stringWithFormat:@"%@%@/%@/", URL_APP_ROOT, [webViewData objectForKey:@"controller"], CLIENT_ID];
#warning APPの設定が出来るまで仮
    //webViewURL = @"http://mc.i-post.jp/nagazoe/spost/tokyo/garally/index.php";

    NSLog(@"Preview For %@", webViewURL);

    NSURL *URL = [NSURL URLWithString:webViewURL];
    URLreq = [NSURLRequest requestWithURL:URL];
    [self.webView loadRequest: URLreq];
    
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    //scheme:appで画面遷移
    if([[[request URL] scheme] isEqualToString:@"app"]){
        
        NSString *viewName = [NSString stringWithFormat:@"%@", [request URL].host ];
        
        UIBarButtonItem *back_button = [[UIBarButtonItem alloc] initWithTitle:@"戻る" style:UIBarButtonItemStylePlain target:nil action:nil];
        
        self.navigationItem.backBarButtonItem = back_button;
        
        [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:viewName] animated:YES];
        
        return NO;
    }
    //scheme:nativeでメソッド呼び出し
    else if ([[[request URL] scheme] isEqualToString:@"native"]){
        NSString *requestMethod = [NSString stringWithFormat:@"%@", [request URL].host ];

        if([requestMethod isEqualToString:@"scroll_disable"]){
            [self scrollDisable];
        }
        else if ([requestMethod isEqualToString:@"page_next"]){
            [self pageNext];
        }
        else if([requestMethod isEqualToString:@"page_back"]){
            [self pageBack];
        }
        return NO;
    }
    else{
        return YES;
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    if(([[error domain]isEqual:NSURLErrorDomain]) && ([error code]!=NSURLErrorCancelled)) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"サーバーに接続できませんでした。\nインターネットに接続されているか確認して下さい。" delegate:self cancelButtonTitle:@"閉じる" otherButtonTitles:@"リトライ", nil];
        
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self.webView stopLoading];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            break;
        case 1:
            [self.webView loadRequest:URLreq];
            break;
    }
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    NSString *javascript = @"navigationBarHidden();";
    [self.webView stringByEvaluatingJavaScriptFromString:javascript];
    
    if (self.webView.canGoBack) {
        UIBarButtonItem *backBarButtonItem =
        [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:BACK_BUTTON_IMG]
                                         style:UIBarButtonItemStylePlain
                                        target:self
         
                                        action:@selector(pageBack)];
        self.navigationItem.leftBarButtonItem = backBarButtonItem;
    }
    else{
        self.navigationItem.leftBarButtonItem = nil;
    }
}

/*
 scheme:native
 */

//webviewのスクロール無効
- (void)scrollDisable
{
    self.webView.scrollView.scrollEnabled = NO;
}

//次のページを表示する。
- (void)pageNext{
    [self.webView goForward];
}

//前のページを表示する。
- (void)pageBack
{
    [self.webView goBack];
}
/*
- (void)pageCheck
{
    [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"pagingCheckFunc([%d, %d]);", [self.webView canGoBack], [self.webView canGoForward]]];
}
*/
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
