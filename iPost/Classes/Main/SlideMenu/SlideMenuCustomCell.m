//
//  SlideMenuCustomCell.m
//  iPost.mock.v4
//
//  Created by ヒロ企画 on 2014/04/24.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "SlideMenuCustomCell.h"

@implementation SlideMenuCustomCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
