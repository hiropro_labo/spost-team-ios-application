//
//  SlideViewController.h
//  iPost.mock.v4
//
//  Created by ヒロ企画 on 2014/04/22.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainWebViewController.h"

@interface SlideViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UITabBarDelegate, UIToolbarDelegate>
@property (weak, nonatomic) IBOutlet UITabBar *tabBar;

@end
