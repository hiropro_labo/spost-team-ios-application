//
//  SlideViewController.m
//  iPost.mock.v4
//
//  Created by ヒロ企画 on 2014/04/22.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "SlideViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "NavigationController.h"
#import "IPMainMenuModel.h"
#import "SlideMenuCustomCell.h"
#import "MainWebViewController.h"


#define MENU_CUSTOMCELL @"SlideMenuCustomCell"

@interface SlideViewController ()
{
    UITableViewController *menuTableViewController;
    IPMainMenuModel   *_model;
    NSArray *_menuJson;
    NSMutableArray *menuArray;
    NSURL *_url;
    NSDictionary *tmp;
    
    NSString *_parent; // 親データのカテゴリID
    NSUInteger *_index; // テーブルID
    
    NSDictionary *socialListArray;
    NSArray *socialKeys;
}

@property (nonatomic, assign) BOOL isOpen;
@property (nonatomic, retain) IBOutlet UIView *dummyView;

@end

@implementation SlideViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // MainMenu Model の読み込み
    if ( ! _model || _model == nil) {
        _model = [[IPMainMenuModel alloc] init];
    }
    
    if ([_model loadListMenu] == nil) {
        [_model download:YES block:nil];
    }
    
    [self setUpSlideMenu];
    
    [self setUpTabMenu];
    
    
    UIViewController *viewController1 = [self.storyboard instantiateViewControllerWithIdentifier:@"top"];
    [self setFirstViewController:viewController1];
    
    [self jsonLoad];
}

- (void)setUpSlideMenu
{
    //mainView
    menuTableViewController = [[UITableViewController alloc] init];
    menuTableViewController.tableView.delegate = self;
    menuTableViewController.tableView.dataSource = self;
    
    //スライドメニューの設定
    [menuTableViewController.view setFrame:_dummyView.frame];
    [self addChildViewController:menuTableViewController];
    [menuTableViewController didMoveToParentViewController:self];
    [self.view addSubview:menuTableViewController.view];
    menuTableViewController.tableView.backgroundColor = [UIColor blackColor];
    menuTableViewController.tableView.separatorColor = [UIColor clearColor];
    
    
    //フッターの設定
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 62)];
    menuTableViewController.tableView.tableFooterView = footerView;
    
    //navigationController
    UINavigationController *navigationController = [[NavigationController alloc] init];
    [self addChildViewController:navigationController];
    [navigationController didMoveToParentViewController:self];
    [self.view addSubview:navigationController.view];
    
    //スライドした時のドロップシャドウ
    navigationController.view.layer.shadowOpacity = 0.3;
    navigationController.view.layer.shadowOffset = CGSizeMake(10.0, 10.0);
    navigationController.view.layer.cornerRadius = 10;
    
    //ソーシャルのボタン設定
    [self setUpSocialButtons:15];
    
}
- (void)setUpSocialButtons:(CGFloat)footerOrignY
{
    
    socialListArray = [_model loadListSocial];
    
    if(socialListArray.count > 0){
        socialKeys = [socialListArray allKeys];
        int i = 0;
        for (id key in socialKeys) {
            
            
            UIButton *socialButton =
            [UIButton buttonWithType:UIButtonTypeRoundedRect];
            socialButton.tag = i;
            
            i++;
            socialButton.frame = CGRectMake(200 - (40 * i), footerOrignY, 30, 30);
            
            [socialButton addTarget:self action:@selector(socialButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            
            
            if([key isEqualToString:@"twitter"]){
                [socialButton setBackgroundImage:[UIImage imageNamed:@"icon_twitter.png"] forState:UIControlStateNormal];
            }
            else if([key isEqualToString:@"facebook"]){
                [socialButton setBackgroundImage:[UIImage imageNamed:@"icon_facebook.png"] forState:UIControlStateNormal];
            }
            else if([key isEqualToString:@"youtube"]){
                [socialButton setBackgroundImage:[UIImage imageNamed:@"icon_youtube.png"] forState:UIControlStateNormal];
            }
            
            [menuTableViewController.tableView.tableFooterView addSubview:socialButton];
            
        }
    }
}

- (void)socialButtonAction:(UIButton *)sender
{
    NSString *urlString = [socialListArray objectForKey:[socialKeys objectAtIndex:sender.tag]];
    
    NSURL* url = [[NSURL alloc] initWithString:urlString];
    
    [[UIApplication sharedApplication] openURL:url];
}
- (void)setUpTabMenu
{
    //デリゲート設定
    self.tabBar.delegate = self;
    [self.view bringSubviewToFront:self.tabBar];
    //選択時の色
    [self.tabBar setTintColor:[UIColor lightGrayColor]];
    
    //画像の色が飛ばされちゃうのでレンダリングモードの設定
    for (int i = 0; i < self.tabBar.items.count; i++) {
        UITabBarItem *tmpItem = self.tabBar.items[i];
        tmpItem.image = [tmpItem.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [menuTableViewController.tableView registerNib:[UINib nibWithNibName:@"SlideMenuCustomCell" bundle:nil] forCellReuseIdentifier:MENU_CUSTOMCELL];
}

#pragma mark - setFirstViewController
-(void)setFirstViewController:(UIViewController *)viewController
{
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]
                                           initWithImage:[UIImage imageNamed:@"button_menu.png"]
                                          style:UIBarButtonItemStylePlain
                                          target:self
                                          action:@selector(slide:)];
    [viewController.navigationItem setRightBarButtonItem:rightBarButtonItem];
    UINavigationController *navigationController = [self.childViewControllers lastObject];
    [navigationController setViewControllers:[NSArray arrayWithObjects:viewController, nil]];
}

-(void)removeFirstViewController
{
    UINavigationController *navigationController = [self.childViewControllers lastObject];
    NSMutableArray *viewControllers = [NSMutableArray arrayWithArray:navigationController.viewControllers];
    [viewControllers removeAllObjects];
    navigationController.viewControllers = viewControllers;
}

- (void)jsonLoad
{
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = @"Loading...";
    
    [hud showAnimated:YES whileExecutingBlock:^{
        _menuJson = (NSArray *)[_model loadListMenu];
        menuArray = [[NSMutableArray alloc] init];
        
        for (NSDictionary *obj in _menuJson) {
            if (obj != nil)
            {
                [menuArray addObject:obj];
            }
        }
        
        NSSortDescriptor *sortDispNo = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];
        NSArray *sortDescArray = [NSArray arrayWithObjects:sortDispNo, nil];
        [menuArray sortUsingDescriptors:sortDescArray];
        
        [menuTableViewController.tableView reloadData];
        
    } completionBlock:^{
        [hud removeFromSuperview];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [menuArray count];
}

/**
 * テーブル全体のセクションの数を返す
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SlideMenuCustomCell *cell = [menuTableViewController.tableView dequeueReusableCellWithIdentifier:MENU_CUSTOMCELL forIndexPath:indexPath];
    
    cell.textLabel.text = menuArray[indexPath.row][JSON_KEY_MENU_NAME];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font = [UIFont boldSystemFontOfSize:14];
    
    // 画像ダウンロード
//    NSString *imgURL = URL_IMG_MAIN_MENU;
//    if (_parent != nil || [_parent length] != 0) {
//        imgURL = URL_IMG_MAIN_MENU;
//    }
    
    NSString *strURL = [NSString stringWithFormat:@"%@%@",
                        URL_IMG_MENU_ICON,
                        menuArray[indexPath.row][JSON_KEY_MENU_ICON]];
    
    NSURL *urlImage = [NSURL URLWithString:strURL];
    
    [cell.imageView setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:IMAGE_PLACEHOLDERIMAGE_MENU2]];

    cell.backgroundColor = [UIColor clearColor];
    cell.imageView.backgroundColor = [UIColor clearColor];
    
    if(indexPath.row % 2 == 0){
        cell.backgroundColor = RGB(38, 38, 38);
        cell.imageView.backgroundColor = RGB(38, 38, 38);
    }
    
    return cell;
}

#pragma mark - UITableViewControllerDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self removeFirstViewController];
    
    /*
    if([menuArray[indexPath.row][@"viewtype"] isEqualToString:@"1"]){
        NSString *viewName = [NSString stringWithFormat:@"%@", menuArray[indexPath.row][@"controller"]];
        [self setFirstViewController:[self.storyboard instantiateViewControllerWithIdentifier:viewName]];
    }*/

    NSString *controllerName = menuArray[indexPath.row][@"controller"];

    if([controllerName isEqualToString:@"top"] ||
       [controllerName isEqualToString:@"news"] ||
       [controllerName isEqualToString:@"schedule"] ||
       [controllerName isEqualToString:@"result"] ||
       [controllerName isEqualToString:@"team"] ||
       [controllerName isEqualToString:@"ticket"] ||
       [controllerName isEqualToString:@"cheer"] ||
       [controllerName isEqualToString:@"setting"] ||
        [controllerName isEqualToString:@"sponsor"])
    {
        [self setFirstViewController:[self.storyboard instantiateViewControllerWithIdentifier:controllerName]];
    }
    else{
        [self setWebViewData:(int)indexPath.row];
        [self setFirstViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"mainWebView"]];
    }
    
    [self slide:nil];
}

//TabBarItemタップの挙動(すべてネイティブのコントローラー)
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    [self removeFirstViewController];
    
    NSString *viewName;
    
    switch (item.tag) {
        case 0:
            viewName = @"top";
            break;
        case 1:
            viewName = @"schedule";
            break;
        case 2:
            viewName = @"ticket";
            break;
        case 3:
            viewName = @"team";
            break;
        case 4:
            viewName = @"result";
            break;
        default:
            break;
    }
    
    [self setFirstViewController:[self.storyboard instantiateViewControllerWithIdentifier:viewName]];
    
}

- (void)setWebViewData:(int)index
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:menuArray[index] forKey:@"webViewData"];
    [ud synchronize];
}

#pragma mark - slide
-(void)slide:(id)sender
{
    _isOpen = !_isOpen;
    
    if(_isOpen == YES) [menuTableViewController.tableView reloadData];
    
    UINavigationController *firstNavigationController = [self.childViewControllers objectAtIndex:1];

    UIView *firstView = firstNavigationController.view;
    [UIView animateWithDuration:0.3 animations:^{
        CGFloat originX = _isOpen ? -200 : 0;
        CGRect frame = firstView.frame;
        frame.origin.x = originX;
        firstView.frame = frame;
        
        //TabBarも同時にスライド
        CGRect tabFrame = self.tabBar.frame;
        tabFrame.origin.x = originX;
        self.tabBar.frame = tabFrame;
    } completion:^(BOOL finished){
        
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
