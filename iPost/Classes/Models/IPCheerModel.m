//
//  IPCheerModel.m
//  sPost.team.mock.v1
//
//  Created by ヒロ企画 on 2014/05/12.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "IPCheerModel.h"

@implementation IPCheerModel

#pragma mark - connection
- (void)download:(BOOL)check block:(void (^)())block
{
    [IPHTTPLoader loadDataWithURL:[NSURL URLWithString:URL_API_CHEER]
                     requestParam:nil
                          success:^(AFHTTPRequestOperation *operation, id resultJson)
     {
         TRACE(@"success!");
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         [self save:resultJson operation:operation];
         for (NSDictionary *dic in resultJson) {
             [self setData:dic];
         }
     }
                          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         TRACE(@"App.net Error : %@", [error localizedDescription]);
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         [IPUtility connectionAlertShow];
     }];
    [self wait];
}


/**
 * JSON を NSUserDefaults に保存
 */
- (void)save:(id)json operation:(AFHTTPRequestOperation *)operation
{
    defaults = [NSUserDefaults standardUserDefaults];
    
    NSDate *jsonTime = [NSDate date];
    
    TRACE(@"JSON SAVE : %@", jsonTime);
    // JSON 保存 親カテゴリー
    [defaults setObject:jsonTime forKey:DEF_CHEER_JSON_TIME];
    [defaults setObject:operation.responseString forKey:DEF_CHEER_JSON];
    [defaults synchronize];
}


/**
 * NSUserDefaults から JSON を読み込む
 */
- (NSDictionary *)load
{
    defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *strJson  = [defaults objectForKey:DEF_CHEER_JSON];
    
    NSData *jsonData;
    NSError *error;
    NSDictionary *json;
    
    @try {
        jsonData = [strJson dataUsingEncoding:NSUnicodeStringEncoding];
        json = [NSJSONSerialization JSONObjectWithData:jsonData
                                               options:NSJSONReadingAllowFragments
                                                 error:&error];
    }
    @catch (NSException *exception) {
        TRACE(@"menu model load catch");
        return nil;
    }
    
    return json;
}


/**
 * NSUserDefaults から JSON を読み込む
 */
- (NSDictionary *)loadTop
{
    NSDictionary *jsonDic = [self load];
    return [jsonDic objectForKey:JSON_KEY_CHEER_TOP];
}


/**
 * NSUserDefaults から JSON を読み込む
 */
- (NSDictionary *)loadList
{
    NSDictionary *jsonDic = [self load];
    return [jsonDic objectForKey:JSON_KEY_CHEER_LIST];
}

@end
