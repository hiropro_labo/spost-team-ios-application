//
//  IPMainMenuModel.h
//  iPost.mock.v4
//
//  Created by ヒロ企画 on 2014/04/23.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "IPModel.h"
#import "IPHTTPLoader.h"


#define JSON_KEY_LIST_MENU          @"list_menu"
#define JSON_KEY_LIST_SOCIAL        @"list_social"

// JSONインデックスキー
#define JSON_KEY_MENU_CONTROLLER    @"controller"
#define JSON_KEY_MENU_NAME          @"name"
#define JSON_KEY_MENU_POS           @"position"
#define JSON_KEY_MENU_ICON          @"icon"
#define JSON_KEY_MENU_VIEWTYPE      @"viewtype"


@interface IPMainMenuModel : IPModel

- (void)download:(BOOL)check block:(void (^)())block;
- (NSDictionary *)loadListSocial;
- (NSDictionary *)loadListMenu;
@end
