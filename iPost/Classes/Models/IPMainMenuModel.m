//
//  IPMainMenuModel.m
//  iPost.mock.v4
//
//  Created by ヒロ企画 on 2014/04/23.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "IPMainMenuModel.h"

@interface IPMainMenuModel () {
    NSString *_strRevision;
    NSNumber *_numLimitStart;
}
@end

@implementation IPMainMenuModel

#pragma mark - connection
- (void)download:(BOOL)check block:(void (^)())block
{
    [IPHTTPLoader loadDataWithURL:[NSURL URLWithString:URL_API_MAINMENU]
                     requestParam:nil
                          success:^(AFHTTPRequestOperation *operation, id resultJson)
     {
         TRACE(@"success!");
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         [self save:resultJson operation:operation];
     }
                          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         TRACE(@"App.net Error : %@", [error localizedDescription]);
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         [IPUtility connectionAlertShow];
     }];
    [self wait];
}



/**
 * JSON を NSUserDefaults に保存
 */
- (void)save:(id)json operation:(AFHTTPRequestOperation *)operation
{
    defaults = [NSUserDefaults standardUserDefaults];
    
    NSDate *jsonTime = [NSDate date];
    
    TRACE(@"JSON SAVE : %@", jsonTime);
    // JSON 保存
    [defaults setObject:jsonTime forKey:DEF_MAINMENU_JSON_TIME];
    [defaults setObject:operation.responseString forKey:DEF_MAINMENU_JSON];
}

- (NSDictionary *)loadListMenu
{
    NSDictionary *jsonDic = [self load:DEF_MAINMENU_JSON];
    return [jsonDic objectForKey:JSON_KEY_LIST_MENU];
}

/**
 * NSUserDefaults から JSON を読み込む
 */
- (NSDictionary *)loadListSocial
{
    NSDictionary *jsonDic = [self load:DEF_MAINMENU_JSON];
    return [jsonDic objectForKey:JSON_KEY_LIST_SOCIAL];
}
@end
