//
//  IPMainModel.h
//  iPost
//
//  Created by 藤田　龍 on 2013/08/27.
//  Copyright (c) 2013年 hiropro. All rights reserved.
//

/*
 * データ格納内容
 *  _arrayImageInfo  : 画面上部の画像情報をNSDictionary形式で格納
 *  _arrayImages     : 上記の画像データをUIImageで格納
 *  _arrayData       : お店情報をNSDictionaryで格納
 */

#import "IPModel.h"
#import "IPHTTPLoader.h"


// JSONインデックスキー
#define JSON_KEY_MAIN_SHOP   @"profile"
#define JSON_KEY_MAIN_IMAGE  @"list"

/// お店情報
#define JSON_KEY_MAIN_SHOP_ID        @"id"
#define JSON_KEY_MAIN_SHOP_CLIENTID  @"client_id"
#define JSON_KEY_MAIN_SHOP_SHOPNAME  @"shop_name"
#define JSON_KEY_MAIN_SHOP_EMAIL     @"email"
#define JSON_KEY_MAIN_SHOP_TEL1      @"tel1"
#define JSON_KEY_MAIN_SHOP_TEL2      @"tel2"
#define JSON_KEY_MAIN_SHOP_TEL3      @"tel3"
#define JSON_KEY_MAIN_SHOP_FAX1      @"fax1"
#define JSON_KEY_MAIN_SHOP_FAX2      @"fax2"
#define JSON_KEY_MAIN_SHOP_FAX3      @"fax3"
#define JSON_KEY_MAIN_SHOP_ZIPCODE1  @"zip_code1"
#define JSON_KEY_MAIN_SHOP_ZIPCODE2  @"zip_code2"
#define JSON_KEY_MAIN_SHOP_PREF      @"pref"
#define JSON_KEY_MAIN_SHOP_CITY      @"city"
#define JSON_KEY_MAIN_SHOP_ADDRESS1  @"address_opt1"
#define JSON_KEY_MAIN_SHOP_ADDRESS2  @"address_opt2"
#define JSON_KEY_MAIN_SHOP_URL       @"url"
#define JSON_KEY_MAIN_SHOP_ONLINE    @"online_shop"
#define JSON_KEY_MAIN_SHOP_OPEN      @"open_hours"
#define JSON_KEY_MAIN_SHOP_HOLIDAY   @"holiday"
#define JSON_KEY_MAIN_SHOP_UPDATED   @"updated_at"
#define JSON_KEY_MAIN_SHOP_CREATED   @"created_at"
#define JSON_KEY_MAIN_SHOP_FILE      @"file_name"
#define JSON_KEY_MAIN_SHOP_TOPIC     @"topic"

/// 画像情報
#define JSON_KEY_MAIN_IMAGE_ID         @"id"
#define JSON_KEY_MAIN_IMAGE_CLIENTID   @"client_id"
#define JSON_KEY_MAIN_IMAGE_FILE       @"file_name"
#define JSON_KEY_MAIN_IMAGE_TEMPFILE   @"tmp_file_name"
#define JSON_KEY_MAIN_IMAGE_LINK       @"link"
#define JSON_KEY_MAIN_IMAGE_STARTTIME  @"start_datetime"
#define JSON_KEY_MAIN_IMAGE_ENDTIME    @"end_datetime"
#define JSON_KEY_MAIN_IMAGE_UPDATED    @"updated_at"
#define JSON_KEY_MAIN_IMAGE_CREATED    @"created_at"



@interface IPMainModel : IPModel

- (void)download:(BOOL)check block:(void (^)())block;

@end
