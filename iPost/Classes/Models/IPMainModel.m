//
//  IPMainModel.m
//  iPost
//
//  Created by 藤田　龍 on 2013/08/27.
//  Copyright (c) 2013年 hiropro. All rights reserved.
//

#import "IPMainModel.h"


@interface IPMainModel () {
    NSString *_strRevision;
}
@end


@implementation IPMainModel

#pragma mark - connection

/**
 * JSON をダウンロード
 */
- (void)download:(BOOL)check block:(void (^)())block
{
    [IPHTTPLoader loadDataWithURL:[NSURL URLWithString:URL_API_MAIN]
                     requestParam:nil
                          success:^(AFHTTPRequestOperation *operation, id resultJson)
                          {
                              TRACE(@"success!");
                              [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                              [self save:resultJson operation:operation];
                          }
                          failure:^(AFHTTPRequestOperation *operation, NSError *error)
                          {
                              TRACE(@"App.net Error : %@", [error localizedDescription]);
                              [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                              [IPUtility connectionAlertShow];
                          }];
    [self wait];
}


/**
 * JSON を NSUserDefaults に保存
 */
- (void)save:(id)json operation:(AFHTTPRequestOperation *)operation
{
    defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *jsonTime = [[[self load:DEF_MAIN_JSON] objectForKey:JSON_KEY_MAIN_SHOP] objectForKey:JSON_KEY_MAIN_SHOP_UPDATED];
    
    TRACE(@"JSON SAVE : %@", jsonTime);
    
    // JSON 保存
    [defaults setObject:jsonTime forKey:DEF_MAIN_JSON_TIME];
    [defaults setObject:operation.responseString forKey:DEF_MAIN_JSON];
}


/**
 * 画像ダウンロード
 */
- (void)loadImage:(void (^)())block
{
    for (NSDictionary *dic in [self getImageInfo]) {
        NSString *strURL = [NSString stringWithFormat:@"%@%@",
                            URL_IMG_MAIN,
                            [dic objectForKey:JSON_KEY_MAIN_IMAGE_FILE]];
        NSURL *url = [NSURL URLWithString:strURL];
        TRACE(@"URL : %@", url);
        
        [IPHTTPLoader loadImageWithURL:url
                               success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *IMG) {
                                   [self setImage:IMG];
                                   block();
                               }
                               failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                   TRACE(@"App.net Error: %@", [error localizedDescription]);
                                   [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                               }];
    }
}
@end
