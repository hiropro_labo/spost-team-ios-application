//
//  IPMainteModel.h
//  iPost
//
//  Created by 藤田　龍 on 2013/08/27.
//  Copyright (c) 2013年 hiropro. All rights reserved.
//

/*
 * データ格納内容
 *  _arrayImageInfo  : 画面上部の画像情報をNSDictionary形式で格納
 *  _arrayImages     : 上記の画像データをUIImageで格納
 *  _arrayData       : お店情報をNSDictionaryで格納
 */

#import "IPModel.h"
#import "IPHTTPLoader.h"


// JSONインデックスキー
#define JSON_KEY_MAINTENANCE_VALUE        @"maintenance"
#define JSON_KEY_MAINTENANCE_SHOPSTATUS   @"shop_status"

#define JSON_KEY_MAINTE_ID                @"id"
#define JSON_KEY_MAINTE_CLIENTID          @"client_id"
#define JSON_KEY_MAINTE_STATUS_CODE       @"status"
#define JSON_KEY_MAINTE_EXPIRED           @"expired_at"
#define JSON_KEY_MAINTE_UPDATED           @"updated_at"


@interface IPMainteModel : IPModel

- (void)download:(BOOL)check block:(void (^)())block;
- (NSDictionary *)load;

@end
