//
//  IPMainModel.m
//  iPost
//
//  Created by 藤田　龍 on 2013/08/27.
//  Copyright (c) 2013年 hiropro. All rights reserved.
//

#import "IPMainteModel.h"


@interface IPMainteModel ()
@end


@implementation IPMainteModel

#pragma mark - connection
/**
 * JSON を サーバーから読み込む
 */
- (void)download:(BOOL)check block:(void (^)())block
{
    [IPHTTPLoader loadDataWithURL:[NSURL URLWithString:URL_API_MAINTE]
                     requestParam:nil
                          success:^(AFHTTPRequestOperation *operation, id resultJson)
                          {
                              TRACE(@"success!");
                              [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                              [self save:resultJson operation:operation];
                          }
                          failure:^(AFHTTPRequestOperation *operation, NSError *error)
                          {
                              TRACE(@"App.net Error : %@", [error localizedDescription]);
                              [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                              [IPUtility connectionAlertShow];
                          }
     ];
    [self wait];
}


/**
 * JSON を NSUserDefaults に保存
 */
- (void)save:(NSDictionary *)json operation:(AFHTTPRequestOperation *)operation
{
    defaults = [NSUserDefaults standardUserDefaults];

    NSDictionary *shop_status = [json objectForKey:JSON_KEY_MAINTENANCE_SHOPSTATUS];
    
    NSString *jsonTime = [shop_status objectForKey:JSON_KEY_MAINTE_UPDATED];
    NSString *defTime = [defaults objectForKey:DEF_MAINTE_JSON_TIME];
    
    // String => Date変換
    NSDate *mainteDate = [self stringToDate:jsonTime];
    NSDate *defDate = [self stringToDate:defTime];
    
    if ( ! defTime && defTime == nil)
    {
        TRACE(@"JSON SAVE : %@", jsonTime);
        // JSON 保存
        [defaults setObject:jsonTime forKey:DEF_MAINTE_JSON_TIME];
        [defaults setObject:operation.responseString forKey:DEF_MAINTE_JSON];
        [defaults synchronize];
    }
    else
    {
        if ( ! [mainteDate isEqualToDate:defDate]) {
            TRACE(@"JSON SAVE : %@ != %@", jsonTime, defTime);
            
            // JSON 保存
            [defaults setObject:jsonTime forKey:DEF_MAINTE_JSON_TIME];
            [defaults setObject:operation.responseString forKey:DEF_MAINTE_JSON];
            [defaults synchronize];
        }
        else
        {
            TRACE(@"JSON NO SAVE");
        }
    }
}


/**
 * NSUserDefaults から JSON を読み込む
 */
- (NSDictionary *)load
{
    defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *strJson  = [defaults objectForKey:DEF_MAINTE_JSON];
    
    NSData *jsonData;
    NSError *error;
    NSDictionary *json;
    
    @try {
        jsonData   = [strJson dataUsingEncoding:NSUnicodeStringEncoding];
        
        json = [NSJSONSerialization JSONObjectWithData:jsonData
                                               options:NSJSONReadingAllowFragments
                                                 error:&error];
    }
    @catch (NSException *exception) {
        TRACE(@"%@", exception);
        return nil;
    }
    
    return json;
}

@end
