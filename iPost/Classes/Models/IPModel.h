//
//  IPModel.h
//  iPost
//
//  Created by 藤田　龍 on 2013/08/27.
//  Copyright (c) 2013年 hiropro. All rights reserved.
//

#import <Foundation/Foundation.h>

#define JSON_KEY_REVISON_CILENT_ID  @"client_id"
#define JSON_KEY_REVISON_COUPON     @"coupon"
#define JSON_KEY_REVISON_ID         @"id"
#define JSON_KEY_REVISON_MAIN       @"main"
#define JSON_KEY_REVISON_MENU       @"menu"
#define JSON_KEY_REVISON_NEWS       @"news"

@interface IPModel : NSObject{
    NSUserDefaults *defaults;
}

- (void)setImageInfoWithDic:(NSDictionary *)dic;
- (NSInteger)numberOfImageInfo;
- (NSMutableArray *)getImageInfo;

- (void)setImage:(UIImage *)image;
- (NSInteger)numberOfImages;
- (UIImage *)getImageAtIndex:(NSInteger)index;

- (void)setData:(NSDictionary *)dic;
- (NSInteger)numberOfData;
- (NSDictionary *)getDataAtIndex:(NSInteger)index;

- (NSDate *)stringToDate:(NSString *)intputDateStr;

- (NSDictionary *)load:(NSString *)def_json_name;

- (void)wait;
@end
