//
//  IPModel.m
//  iPost
//
//  Created by 藤田　龍 on 2013/08/27.
//  Copyright (c) 2013年 hiropro. All rights reserved.
//

#import "IPModel.h"


@interface IPModel () {
    NSMutableArray *_arrayImageInfo; // サーバから取得した画像情報をNSDictionary形式で格納
    NSMutableArray *_arrayImages; // 画像データをUIImageで格納
    NSMutableArray *_arrayData; // リスト情報をNSDictionaryで格納
}
@end


@implementation IPModel

- (id)init
{
    if (!self) {
        self = [super init];

        _arrayImageInfo = [[NSMutableArray alloc] init];
        _arrayImages = [[NSMutableArray alloc] init];
        _arrayData = [[NSMutableArray alloc] init];
    }
    
    return self;
}


- (void)setImageInfoWithDic:(NSDictionary *)dic
{
    if (!_arrayImageInfo) {
        _arrayImageInfo = [[NSMutableArray alloc] init];
    }
    [_arrayImageInfo addObject:dic];
}


- (NSInteger)numberOfImageInfo
{
    return [_arrayImageInfo count];
}


- (NSMutableArray *)getImageInfo
{
    return _arrayImageInfo;
}


- (void)setArrayData
{
}


- (void)setImage:(UIImage *)image
{
    //TRACE(@"_arrayImages = %@", _arrayImages);
    TRACE(@"image w = %f, h = %f", image.size.width, image.size.height);
    if (!_arrayImages) {
        _arrayImages = [[NSMutableArray alloc] init];
    }
    
    if (image != nil) {
        [_arrayImages addObject:image];
    }
    //TRACE(@"_arrayImages = %@", _arrayImages);
}


- (NSInteger)numberOfImages
{
    return [_arrayImages count];
}


- (UIImage *)getImageAtIndex:(NSInteger)index
{
    TRACE(@"index = %ld, [_arrayImages count] = %ld", (long)index, (unsigned long)[_arrayImages count]);
    UIImage *resImage;
    if (_arrayImages != nil && index <= [_arrayImages count] - 1) {
        resImage = [_arrayImages objectAtIndex:index];
    } else {
        resImage = nil;
    }
    
    return resImage;
}


- (void)setData:(NSDictionary *)dic
{
    if (!_arrayData) {
        _arrayData = [[NSMutableArray alloc] init];
    }
    
    if (dic != nil) {
        [_arrayData addObject:dic];
    }
}


- (NSInteger)numberOfData
{
    NSInteger cnt = [_arrayData count];
    
    return cnt;
}


- (NSDictionary *)getDataAtIndex:(NSInteger)index
{
    NSDictionary *resDic = [_arrayData objectAtIndex:index];
    
    return resDic;
}


/**
 * String => Date変換
 */
- (NSDate *)stringToDate:(NSString *)intputDateStr
{
    NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
	[inputDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [inputDateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	NSDate *inputDate = [inputDateFormatter dateFromString:intputDateStr];
    
    return inputDate;
}


/**
 * NSUserDefaults から JSON を読み込む
 */
- (NSDictionary *)load:(NSString *)def_json_name
{
    defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *strJson  = [defaults objectForKey:def_json_name];
    NSData *jsonData;
    NSError *error;
    NSMutableDictionary *json;
    
    TRACE(@"defaults to json load : %@", def_json_name);
    @try {
        jsonData  = [strJson dataUsingEncoding:NSUnicodeStringEncoding];
        json = [NSJSONSerialization JSONObjectWithData:jsonData
                                               options:NSJSONReadingAllowFragments
                                                 error:&error];
    }
    @catch (NSException *exception) {
        TRACE(@"defaults to json load catch");
        return nil;
    }
    
    return json;
}


/**
 * ダウンロード中、少し待つようにする
 */
- (void)wait
{
    TRACE(@"WAIT...");
    
    int cnt = 0;
    while (cnt < 4) {
        // NSConnectionのイベントを処理させる。
        // runUntilDateで0秒後までrun loopを回す。= 1回まわすと同じと考えます。
        [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.0]];
        // 0.1秒間隔
        [NSThread sleepForTimeInterval:0.1];
        cnt ++;
    }
}

@end
