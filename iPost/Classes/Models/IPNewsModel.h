//
//  IPNewsModel.h
//  iPost
//
//  Created by 藤田　龍 on 2013/09/01.
//  Copyright (c) 2013年 hiropro. All rights reserved.
//

#import "IPModel.h"
#import "IPHTTPLoader.h"


// JSONインデックスキー
#define JSON_KEY_MAIN_SHOP   @"profile"
#define JSON_KEY_MAIN_IMAGE  @"list"
#define JSON_KEY_NEWS_ID            @"id"
#define JSON_KEY_NEWS_CLIENTID      @"client_id"
#define JSON_KEY_NEWS_TITLE         @"title"
#define JSON_KEY_NEWS_BODY          @"body"
#define JSON_KEY_NEWS_FILE          @"file_name"
#define JSON_KEY_NEWS_TEMPFILE      @"tmp_file_name"
#define JSON_KEY_NEWS_GOOD          @"good"
#define JSON_KEY_NEWS_COUNT         @"good_cnt"
#define JSON_KEY_NEWS_NOTICE        @"notice"
#define JSON_KEY_NEWS_NOTICESTATUS  @"notice_status"
#define JSON_KEY_NEWS_SEND          @"send_at"
#define JSON_KEY_NEWS_UPDATED       @"updated_at"
#define JSON_KEY_NEWS_CREATED       @"created_at"


@interface IPNewsModel : IPModel

- (void)download:(BOOL)check block:(void (^)())block;

@end
