//
//  IPNewsModel.m
//  iPost
//
//  Created by 藤田　龍 on 2013/09/01.
//  Copyright (c) 2013年 hiropro. All rights reserved.
//

#import "IPNewsModel.h"


@interface IPNewsModel () {
    NSString *_strRevision;
    NSNumber *_numLimitStart;
}
@end


@implementation IPNewsModel

#pragma mark - connection
- (void)download:(BOOL)check block:(void (^)())block
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
#warning インストールの時にtoken取得するまえにここが走ってしまう
    NSDictionary *tokenDictionary;
    
    if ([ud objectForKey:DEF_TOKEN]) {
        tokenDictionary = [NSDictionary dictionaryWithObject:[ud objectForKey:DEF_TOKEN] forKey:@"token"];
    }

    [IPHTTPLoader loadDataWithURL:[NSURL URLWithString:URL_API_NEWS]
                     requestParam:tokenDictionary
                          success:^(AFHTTPRequestOperation *operation, id resultJson)
                          {
                              TRACE(@"success!");
                              [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                              [self save:resultJson operation:operation];
                          }
                          failure:^(AFHTTPRequestOperation *operation, NSError *error)
                          {
                              TRACE(@"App.net Error : %@", [error localizedDescription]);
                              [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                              [IPUtility connectionAlertShow];
                          }];
    [self wait];
}



/**
 * JSON を NSUserDefaults に保存
 */
- (void)save:(id)json operation:(AFHTTPRequestOperation *)operation
{
    defaults = [NSUserDefaults standardUserDefaults];
    
    NSDate *jsonTime = [NSDate date];
    
    TRACE(@"JSON SAVE : %@", jsonTime);
    // JSON 保存
    [defaults setObject:jsonTime forKey:DEF_NEWS_JSON_TIME];
    [defaults setObject:operation.responseString forKey:DEF_NEWS_JSON];
    [defaults synchronize];
}

@end
