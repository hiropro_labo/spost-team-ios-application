//
//  IPResultModel.h
//  sPost.team.mock.v1
//
//  Created by ヒロ企画 on 2014/05/12.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "IPModel.h"
#import "IPHTTPLoader.h"

// JSONインデックスキー
#define JSON_KEY_RESULT_TOP   @"menu_top"
#define JSON_KEY_RESULT_LIST  @"list"

// メニュー トップ画像
#define JSON_KEY_RESULT_TOP_ID         @"id"
#define JSON_KEY_RESULT_TOP_CLIENTID   @"client_id"
#define JSON_KEY_RESULT_TOP_FILE       @"file_name"
#define JSON_KEY_RESULT_TOP_TEMPFILE   @"tmp_file_name"
#define JSON_KEY_RESULT_TOP_STARTTIME  @"start_datetime"
#define JSON_KEY_RESULT_TOP_ENDTIME    @"end_datetime"
#define JSON_KEY_RESULT_TOP_POSITION   @"position"
#define JSON_KEY_RESULT_TOP_UPDATED    @"updated_at"
#define JSON_KEY_RESULT_TOP_CREATED    @"created_at"

// メニュー リスト
#define JSON_KEY_RESULT_LIST_CLIENTID     @"team_id"
#define JSON_KEY_RESULT_LIST_ID           @"id"
#define JSON_KEY_RESULT_LIST_ENABLE       @"enable"
#define JSON_KEY_RESULT_LIST_ENEMY        @"enemy"
#define JSON_KEY_RESULT_LIST_TITLE        @"title"
#define JSON_KEY_RESULT_LIST_TEXT         @"text"
#define JSON_KEY_RESULT_LIST_ENEMYNAME    @"enemy_name"
#define JSON_KEY_RESULT_LIST_TEAMNAME     @"team_name"
#define JSON_KEY_RESULT_LIST_POSITION     @"position"
#define JSON_KEY_RESULT_LIST_WINORLOSE    @"win_or_lose"
#define JSON_KEY_RESULT_LIST_HOMEANDAWAY  @"home_and_away"
#define JSON_KEY_RESULT_LIST_TEAMSCORE    @"team_score"
#define JSON_KEY_RESULT_LIST_ENEMYSCORE   @"enemy_score"
#define JSON_KEY_RESULT_LIST_SCHEDULE     @"game_schedule"
#define JSON_KEY_RESULT_LIST_UPDATED      @"updated_at"
#define JSON_KEY_RESULT_LIST_CREATED      @"created_at"

@interface IPResultModel : IPModel

- (void)download:(BOOL)check block:(void (^)())block;
- (NSDictionary *)loadTop;
- (NSArray *)loadList;

@end
