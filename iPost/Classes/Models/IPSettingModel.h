//
//  IPSettingModel.h
//  iPost.mock.v3
//
//  Created by 北川 義隆 on 2014/03/22.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "IPModel.h"
#import "IPHTTPLoader.h"

#define JSON_KEY_SETTING_ID         @"id"
#define JSON_KEY_SETTING_CLIENT_ID  @"client_id"
#define JSON_KEY_SETTING_TOKEN      @"token"
#define JSON_KEY_SETTING_DEVICE     @"device"
#define JSON_KEY_SETTING_ALLOW_FLG  @"allow_flg"
#define JSON_KEY_SETTING_UPDATED    @"updated_at"
#define JSON_KEY_SETTING_CREATED    @"created_at"

@interface IPSettingModel : IPModel

- (void)download:(BOOL)check block:(void (^)())block;
- (NSArray *)load;

@end
