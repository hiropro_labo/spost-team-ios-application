//
//  IPSettingModel.m
//  iPost.mock.v3
//
//  Created by 北川 義隆 on 2014/03/22.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "IPSettingModel.h"

@implementation IPSettingModel

#pragma mark - connection
/**
 * JSON を サーバーから読み込む
 */
- (void)download:(BOOL)check block:(void (^)())block
{
    NSString *url = [NSString stringWithFormat:@"%@%@", URL_API_SETTING, [[NSUserDefaults standardUserDefaults] stringForKey:DEF_TOKEN]];
    
    TRACE(@"%@", url);
    
    [IPHTTPLoader loadDataWithURL:[NSURL URLWithString:url]
                     requestParam:nil
                          success:^(AFHTTPRequestOperation *operation, id resultJson) {
                              TRACE(@"success!");
                              
                              [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                              [self save:resultJson operation:operation];
                          }
                          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                              TRACE(@"App.net Error : %@", [error localizedDescription]);
                              
                              [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                              [IPUtility connectionAlertShow];
                          }];
}


/**
 * JSON を NSUserDefaults に保存
 */
- (void)save:(NSArray *)array operation:(AFHTTPRequestOperation *)operation
{
    defaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary *json = [array objectAtIndex:0];
    
    NSString *jsonTime = [json objectForKey:JSON_KEY_SETTING_UPDATED];
    
    TRACE(@"JSON SAVE : %@", jsonTime);
    // JSON 保存
    [defaults setObject:jsonTime forKey:DEF_SETTING_JSON_TIME];
    [defaults setObject:operation.responseString forKey:DEF_SETTING_JSON];
}


/**
 * NSUserDefaults から JSON を読み込む
 */
- (NSArray *)load
{
    defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *strJson  = [defaults objectForKey:DEF_SETTING_JSON];
    
    NSData *jsonData;
    NSError *error;
    NSArray *array;
    
    @try {
        jsonData   = [strJson dataUsingEncoding:NSUnicodeStringEncoding];
        
        array = [NSJSONSerialization JSONObjectWithData:jsonData
                                               options:NSJSONReadingAllowFragments
                                                 error:&error];
    }
    @catch (NSException *exception) {
        TRACE(@"%@", exception);
        return nil;
    }
    return array;
}

@end
