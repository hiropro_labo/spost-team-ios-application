//
//  IPSponsorModel.h
//  sPost.team.mock.v1
//
//  Created by ヒロ企画 on 2014/05/20.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "IPModel.h"
#import "IPHTTPLoader.h"

// JSONインデックスキー
#define JSON_KEY_SPONSOR_LIST  @"list"

// メニュー リスト
#define JSON_KEY_SPONSOR_LIST_CLIENTID      @"team_id"
#define JSON_KEY_SPONSOR_LIST_ID            @"id"
#define JSON_KEY_SPONSOR_LIST_ENABLE        @"enable"
#define JSON_KEY_SPONSOR_LIST_APPNAME       @"app_name"
#define JSON_KEY_SPONSOR_LIST_FILE          @"file_name"
#define JSON_KEY_SPONSOR_LIST_SPONSORNAME   @"sponsor_name"
#define JSON_KEY_SPONSOR_LIST_SPONSORID     @"sponsor_id"
#define JSON_KEY_SPONSOR_LIST_APPURLANDROID @"app_url_android"
#define JSON_KEY_SPONSOR_LIST_APPURLIOS     @"app_url_ios"
#define JSON_KEY_SPONSOR_LIST_POSITION      @"position"
#define JSON_KEY_SPONSOR_LIST_UPDATED       @"updated_at"
#define JSON_KEY_SPONSOR_LIST_CREATED       @"created_at"

@interface IPSponsorModel : IPModel

- (void)download:(BOOL)check block:(void (^)())block;

@end

