//
//  IPSponsorModel.m
//  sPost.team.mock.v1
//
//  Created by ヒロ企画 on 2014/05/20.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "IPSponsorModel.h"

@implementation IPSponsorModel

#pragma mark - connection
- (void)download:(BOOL)check block:(void (^)())block
{
    [IPHTTPLoader loadDataWithURL:[NSURL URLWithString:URL_API_SPONSOR]
                     requestParam:nil
                          success:^(AFHTTPRequestOperation *operation, id resultJson)
     {
         TRACE(@"success!");
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         [self save:resultJson operation:operation];
         for (NSDictionary *dic in resultJson) {
             [self setData:dic];
         }
     }
                          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         TRACE(@"App.net Error : %@", [error localizedDescription]);
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         [IPUtility connectionAlertShow];
     }];
    [self wait];
}


/**
 * JSON を NSUserDefaults に保存
 */
- (void)save:(id)json operation:(AFHTTPRequestOperation *)operation
{
    defaults = [NSUserDefaults standardUserDefaults];
    
    NSDate *jsonTime = [NSDate date];
    
    TRACE(@"JSON SAVE : %@", jsonTime);
    // JSON 保存
    [defaults setObject:jsonTime forKey:DEF_SPONSOR_JSON_TIME];
    [defaults setObject:operation.responseString forKey:DEF_SPONSOR_JSON];
    [defaults synchronize];
}


/**
 * NSUserDefaults から JSON を読み込む
 *//*
- (NSDictionary *)load
{
    defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *strJson;
    strJson  = [defaults objectForKey:DEF_SPONSOR_JSON];
    
    NSData *jsonData;
    NSError *error;
    NSDictionary *json;
    
    @try {
        jsonData = [strJson dataUsingEncoding:NSUnicodeStringEncoding];
        json = [NSJSONSerialization JSONObjectWithData:jsonData
                                               options:NSJSONReadingAllowFragments
                                                 error:&error];
    }
    @catch (NSException *exception) {
        TRACE(@"sposor model load catch");
        return nil;
    }
    
    return json;
}*/
/*
 * NSUserDefaults から JSON を読み込む
 *//*
- (NSDictionary *)loadList
{
    NSDictionary *jsonDic = [self load];
    
    return [jsonDic objectForKey:JSON_KEY_SPONSOR_LIST];
}
*/
@end
