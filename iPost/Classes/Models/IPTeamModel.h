//
//  IPTeamModel.h
//  sPost.team.mock.v1
//
//  Created by ヒロ企画 on 2014/05/12.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "IPModel.h"
#import "IPHTTPLoader.h"

// JSONインデックスキー
#define JSON_KEY_TEAM_TOP   @"menu_top"
#define JSON_KEY_TEAM_LIST  @"list"

// メニュー トップ画像
#define JSON_KEY_TEAM_TOP_ID         @"id"
#define JSON_KEY_TEAM_TOP_CLIENTID   @"client_id"
#define JSON_KEY_TEAM_TOP_FILE       @"file_name"
#define JSON_KEY_TEAM_TOP_TEMPFILE   @"tmp_file_name"
#define JSON_KEY_TEAM_TOP_STARTTIME  @"start_datetime"
#define JSON_KEY_TEAM_TOP_ENDTIME    @"end_datetime"
#define JSON_KEY_TEAM_TOP_POSITION   @"position"
#define JSON_KEY_TEAM_TOP_UPDATED    @"updated_at"
#define JSON_KEY_TEAM_TOP_CREATED    @"created_at"

// メニュー リスト
#define JSON_KEY_TEAM_LIST_ID            @"id"
#define JSON_KEY_TEAM_LIST_CLIENTID      @"client_id"
#define JSON_KEY_TEAM_LIST_ENABLE        @"enable"
#define JSON_KEY_TEAM_LIST_PLAYERNAME    @"player_name"
#define JSON_KEY_TEAM_LIST_SUBTITLE      @"play_position"
#define JSON_KEY_TEAM_LIST_UNIFORMNUMBER @"uniform_number"
#define JSON_KEY_TEAM_LIST_BIRTHDAY      @"birth_day"
#define JSON_KEY_TEAM_LIST_HEIGHT        @"height"
#define JSON_KEY_TEAM_LIST_WEIGHT        @"weight"
#define JSON_KEY_TEAM_LIST_ALMAMATER     @"alma_mater"
#define JSON_KEY_TEAM_LIST_FILE          @"file_name"
#define JSON_KEY_TEAM_LIST_TEMPFILE      @"tmp_file_name"
#define JSON_KEY_TEAM_LIST_POSITION      @"position"
#define JSON_KEY_TEAM_LIST_UPDATED       @"updated_at"
#define JSON_KEY_TEAM_LIST_CREATED       @"created_at"

@interface IPTeamModel : IPModel

- (void)download:(BOOL)check block:(void (^)())block;
- (NSDictionary *)loadTop;
- (NSArray *)loadList;

@end
