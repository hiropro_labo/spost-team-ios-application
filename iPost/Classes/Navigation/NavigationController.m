//
//  NavigationController.m
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2013/12/01.
//  Copyright (c) 2013年 北川 義隆. All rights reserved.
//

#import "NavigationController.h"

@interface NavigationController ()

@end

@implementation NavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:NAV_BACKGROUND_IMAGE] forBarMetrics:UIBarMetricsDefault];
        
    } else {
        [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:NAV_BACKGROUND_IMAGE] forBarMetrics:UIBarMetricsDefault];
        [UINavigationBar appearance].titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    }
    
    
    [UINavigationBar appearance].tintColor = [UIColor whiteColor];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
