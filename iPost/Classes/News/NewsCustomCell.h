    //
//  CheerCustomCell.h
//  sPost.team.mock.v1
//
//  Created by ヒロ企画 on 2014/06/20.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsCustomCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UILabel *titileLabel;

@end
