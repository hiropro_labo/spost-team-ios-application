//
//  MenuDetailViewController.h
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2013/10/28.
//  Copyright (c) 2013年 北川 義隆. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IPNewsModel.h"

#define COLOR_DETAIL     RGB(50, 50, 50)
#define DETAIL_FONTSIZE  13.
#define DETAIL_WIDTH     280.

@interface NewsDetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *titleBarView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UITextView *bodyLabel;
@property (weak, nonatomic) IBOutlet UIImageView *newsImageView;
@property (weak, nonatomic) IBOutlet UIButton *goodButton;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;

@property (weak, nonatomic) NSIndexPath *indexPath;

- (void)setData:(NSDictionary *)dicData;
- (NSDictionary *)getData;
- (NSURL *)getUrl;
- (void)setUrl:(NSURL *)str;

@end
