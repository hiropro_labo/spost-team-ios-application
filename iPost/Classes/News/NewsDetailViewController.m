//
//  MenuDetailViewController.m
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2013/10/28.
//  Copyright (c) 2013年 北川 義隆. All rights reserved.
//

#import "NewsDetailViewController.h"
//#import "MainViewController.h"
#import "AFNetworking.h"

@interface NewsDetailViewController () <OHAttributedLabelDelegate>
{
    NSMutableData *responsData;
    IPNewsModel *_model;
    NSMutableDictionary *newsData;
    
    NSURL *_url;
}
@end


@implementation NewsDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {}
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = NAVIGATION_TITLE;
    
    newsData = [[_model getDataAtIndex:0] mutableCopy];
    
    UIBarButtonItem *backBarButtonItem =
    [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:BACK_BUTTON_IMG]
                                     style:UIBarButtonItemStylePlain
                                    target:self.navigationController
     
                                    action:@selector(popViewControllerAnimated:)];
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
}

- (void)viewWillAppear:(BOOL)animated
{
    /// ニュース画像
    if(![newsData objectForKey:JSON_KEY_NEWS_FILE] || [newsData objectForKey:JSON_KEY_NEWS_FILE] == nil || [[newsData objectForKey:JSON_KEY_NEWS_FILE] isEqualToString:@""]){
        //self.titleBarView.frame = CGRectMake(0, 0, self.titleBarView.frame.size.width, self.titleBarView.frame.size.height);
        //self.bodyLabel.frame = CGRectMake(0, self.titleBarView.frame.size.height, self.bodyLabel.frame.size.width, self.bodyLabel.frame.size.height);
        //[[self.scrollView viewWithTag:99] removeFromSuperview];
        self.newsImageView.frame = CGRectMake(0, 0, 0, 0);
    }
    else{
        NSString *strURL = [NSString stringWithFormat:@"%@%@",
                            URL_IMG_NEWS,
                            [newsData objectForKey:JSON_KEY_NEWS_FILE]];
        NSURL *urlImage = [NSURL URLWithString:strURL];
        TRACE(@"urlImage = %@", urlImage);
        [self.newsImageView setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:IMAGE_PLACEHOLDERIMAGE_MENU_DETAIL]];
    }
    
    int intDate;
    
    if([newsData objectForKey:JSON_KEY_NEWS_SEND] != [NSNull null]){
        intDate = [[newsData objectForKey:JSON_KEY_NEWS_SEND] intValue];
    }
    else{
        intDate = [[newsData objectForKey:JSON_KEY_NEWS_UPDATED] intValue];
    }

    NSString *date =
    [IPUtility dateFormatWithJsonTime:[NSString stringWithFormat:@"%d", intDate] format:@"yyyy年MM月dd日"];
    
    //日付
    self.dayLabel.text = date;
    
    //タイトル
    self.titleLabel.text = [newsData objectForKey:JSON_KEY_NEWS_TITLE];
    [self.titleLabel sizeToFit];

    //本文
    self.bodyLabel.text = [newsData objectForKey:JSON_KEY_NEWS_BODY];
    
    //いいねボタン
    [self.goodButton addTarget:self action:@selector(countUp) forControlEvents:UIControlEventTouchUpInside];
    if([[newsData objectForKey:JSON_KEY_NEWS_GOOD] intValue] == 1){
        self.goodButton.enabled = NO;
        [self.goodButton setImage:[UIImage imageNamed:@"btn_good_off.png"] forState:UIControlStateNormal];
    }
    
    //いいねカウント
    self.countLabel.text = [newsData objectForKey:JSON_KEY_NEWS_COUNT];

}

- (void)countUp
{
    NSLog(@"Good!!");
    [self.goodButton setImage:[UIImage imageNamed:@"btn_good_off.png"] forState:UIControlStateNormal];
    self.goodButton.enabled = NO;
    [self sendGoodCount:[newsData objectForKey:JSON_KEY_NEWS_ID]];
}

- (void)sendGoodCount:(NSString *)newsId
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URL_API_SEND_GOOD]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    
    NSString *token = [[NSUserDefaults standardUserDefaults] stringForKey:DEF_TOKEN];
    NSString *data = [NSString stringWithFormat:@"&token=%@&news_id=%@", token, newsId];
    
    [request setHTTPBody:[data dataUsingEncoding:NSUTF8StringEncoding]];
    
    TRACE(@"%@ : POST : %@", request, data);
    
    NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];
    if (connection) {
        TRACE(@"Good SEND : OK");
    } else {
        TRACE(@"Good SEND : ERROR");
    }
}
/*
-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    TRACE(@"");
    
    CGSize size = _scrollView.contentSize;
    size.height = 405;
    _scrollView.contentSize = size;
    
}
*/
/**
 * 全角判断
 * @return bool YES:半角/NO:全角含む
 */
- (BOOL)isAllHalfWidthCharacter:(NSString *)num
{
    NSUInteger nsStringlen = [num length];
    const char *utf8 = [num UTF8String];
    size_t cStringlen = strlen(utf8);
    if (nsStringlen == cStringlen) {
        return YES;
    } else {
        return NO;
    }
}


#pragma mark - public method

- (void)setData:(NSDictionary *)dicData
{
    if (!_model || _model == nil) {
        _model = [[IPNewsModel alloc] init];
    }
    [_model setData:dicData];
}

- (NSDictionary *)getData
{
    NSDictionary *dic = [_model getDataAtIndex:0];
    
    return dic;
}

- (NSURL *)getUrl
{
    return _url;
}

#pragma mark - private method
- (void)setUrl:(NSURL *)str
{
    _url = str;
}


- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    CGFloat fixedWidth = self.bodyLabel.frame.size.width;
    CGSize newSize = [self.bodyLabel sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = self.bodyLabel.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    self.bodyLabel.frame = newFrame;
    
    CGFloat footerStartY = (self.bodyLabel.frame.origin.y + self.bodyLabel.frame.size.height);
    
    CGRect rect = self.newsImageView.frame;
    rect.origin.y = (footerStartY + 16);
    self.newsImageView.frame = rect;
    
    //CGRect screenSize = [[UIScreen mainScreen] applicationFrame];
    //CGFloat maxHeightSize = (screenSize.size.height - 44);
    /*
#warning 多分うまくいってない
    if(maxHeightSize < footerStartY){
        self.goodButtonTitleView.frame = CGRectMake(0, footerStartY, self.titleBarView.frame.size.width, self.titleBarView.frame.size.height);
    }
    else{
        self.titleBarView.frame = CGRectMake(0, (maxHeightSize - self.titleBarView.frame.size.height), self.titleBarView.frame.size.width, self.titleBarView.frame.size.height);
    }
     */
    //navigationBarとstatusBar分足してスクロールビューの高さ
    CGSize size = self.scrollView.contentSize;
    size.height = self.bodyLabel.frame.size.height + 80 + self.newsImageView.frame.size.height;
    
    self.scrollView.contentSize = size;
}

- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // データを初期化
    responsData = [[NSMutableData alloc] initWithData:0];
    
}

// [7]
- (void) connection:(NSURLConnection*)connection didReceiveData:(NSData *)data
{
    // データを追加する
    [responsData appendData:data];
}

// [8]
- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    // エラー処理
    // お好きにどうぞ！
    // --- 省略  ---
}

- (void) connectionDidFinishLoading:(NSURLConnection*)connection
{
    
    NSString *dataString = [[NSString alloc] initWithData:responsData encoding:NSUTF8StringEncoding];
    NSLog(@"%@", dataString);
#warning そのままdataString入れるだけで大丈夫かも
    //int count = [self.countLabel.text intValue];
    [newsData setObject:dataString forKey:JSON_KEY_NEWS_COUNT];
    self.countLabel.text = [newsData objectForKey:JSON_KEY_NEWS_COUNT];
    [_model download:YES block:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
