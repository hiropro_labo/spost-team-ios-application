//
//  MenuViewController.m
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2013/10/26.
//  Copyright (c) 2013年 北川 義隆. All rights reserved.
//

#import "NewsViewController.h"
#import "NewsDetailViewController.h"
#import "IPNewsModel.h"
#import "AFNetworking.h"
#import "NewsCustomCell.h"

#define MENU_CUSTOMCELL @"newsCustomCell"


@interface NewsViewController () {
    
    IBOutlet UIImageView *_imageView;
    IBOutlet UITableView *_tableView;
    
    NSMutableArray *newsArray;
    IPNewsModel   *_model;
    NSDictionary *_newsJson;
}
@end


@implementation NewsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {}
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    TRACE(@"");
    
    self.navigationItem.title = NAVIGATION_TITLE;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    TRACE(@"");
    
    // News Model の読み込み
    if ( ! _model || _model == nil) {
        _model = [[IPNewsModel alloc] init];
    }
    
    [_model download:YES block:nil];
    
    [self jsonLoad];
    
    BOOL isCheck = YES;
    if (0 == [_model numberOfData]) {
        isCheck = NO;
    }
    
    //[self menuImgDownload:isCheck];

    [_tableView registerNib:[UINib nibWithNibName:@"NewsCustomCell" bundle:nil] forCellReuseIdentifier:MENU_CUSTOMCELL];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Json data source

/**
 * NSUserDefaults に保存している JSON 情報を取得する
 */
- (void)jsonLoad
{
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = @"Loading...";
    
    [hud showAnimated:YES whileExecutingBlock:^{
        _newsJson = [_model load:DEF_NEWS_JSON];
        newsArray = [[NSMutableArray alloc] init];
        
        for (NSDictionary *obj in _newsJson) {
            if (obj != nil)
            {
                [newsArray addObject:obj];
            }
        }
        
    } completionBlock:^{
        [hud removeFromSuperview];
        
        [_tableView reloadData];
    }];
}


#pragma mark - Table view data source

/**
 * セルの数
 */
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [newsArray count];
}


/**
 * セルの高さ
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 56;
}


/**
 * テーブルのセル表示処理
 */
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewsCustomCell *cell = [_tableView dequeueReusableCellWithIdentifier:MENU_CUSTOMCELL forIndexPath:indexPath];
    NSDictionary * newsData = [newsArray objectAtIndex:indexPath.row];
    
    int intDate;
    
    if([newsData objectForKey:JSON_KEY_NEWS_SEND] != [NSNull null]){
        intDate = [[newsData objectForKey:JSON_KEY_NEWS_SEND] intValue];
    }
    else{
        intDate = [[newsData objectForKey:JSON_KEY_NEWS_UPDATED] intValue];
    }
    
    NSString *date =
    [IPUtility dateFormatWithJsonTime:[NSString stringWithFormat:@"%d", intDate] format:@"yyyy/MM/dd  EEE"];
    
    cell.dayLabel.text = date;
    
    cell.titileLabel.text = [newsData objectForKey:JSON_KEY_NEWS_TITLE];

    //アクセサリー画像
    UIImage *arrowImage = [UIImage imageNamed:@"arrow.png"];
    UIImageView *arrowImageView = [[UIImageView alloc] initWithImage:arrowImage];
    
    //画像が大きい場合にはみ出さないようにViewの大きさを固定化
    arrowImageView.frame = CGRectMake(0, 0, 10, 15);
    //アクセサリービューにイメージを設定
    cell.accessoryView = arrowImageView;
    
    return cell;
}


#pragma mark - Table view delegate

/**
 * セルの選択
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TRACE(@"TABLE CEL SELECT : %ld", (long)indexPath.row);
    
    [self performSegueWithIdentifier:@"newsDetail" sender:self];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}


#pragma mark - private method

/*
 * サーバからデータ取得するようモデルに要求
 *//*
- (void)menuImgDownload:(BOOL)check
{
    TRACE(@"");
    
    NSDictionary *dic = [_model loadTop];
    
    NSString *strURL;
    strURL = [NSString stringWithFormat:@"%@%@",
            URL_IMG_CHEER_TOP,
            [dic objectForKey:JSON_KEY_CHEER_TOP_FILE]];
    NSURL *urlImage = [NSURL URLWithString:strURL];

    TRACE(@"urlImage = %@", urlImage);
    
    [_imageView setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:IMAGE_PLACEHOLDERIMAGE_MENU1]];
    [_tableView reloadData];
}
*/
- (NSDictionary *)getCurrData
{
    return [newsArray objectAtIndex:[_tableView indexPathForSelectedRow].row];
}

@end