//
//  IPHTTPLoader.h
//  iPost
//
//  Created by 藤田　龍 on 2013/08/24.
//  Copyright (c) 2013年 hiropro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"
#import "AFHTTPClient.h"
#import "AFJSONRequestOperation.h"
#import "AFImageRequestOperation.h"

@interface IPHTTPLoader : NSObject

+ (void)loadDataWithURL:(NSURL *)url requestParam:(NSDictionary *)requestParam success:(void (^)(AFHTTPRequestOperation *operation, id resultJson))success failure:(void (^)(AFHTTPRequestOperation *operation, id resultJson))failure;
+ (void)loadImageWithURL:(NSURL *)url success:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image))success failure:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error))failure;

@end
