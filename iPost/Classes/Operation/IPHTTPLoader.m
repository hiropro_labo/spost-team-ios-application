//
//  IPHTTPLoader.m
//  iPost
//
//  Created by 藤田　龍 on 2013/08/24.
//  Copyright (c) 2013年 hiropro. All rights reserved.
//

#import "IPHTTPLoader.h"


@implementation IPHTTPLoader

+ (void)loadDataWithURL:(NSURL *)url requestParam:(NSDictionary *)requestParam
                success:(void (^)(AFHTTPRequestOperation *, id))success
                failure:(void (^)(AFHTTPRequestOperation *, id))failure
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

    Reachability *reachablity = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachablity currentReachabilityStatus];
    
    AFHTTPClient *httpClient;
    AFJSONRequestOperation *operation;
    NSMutableURLRequest *request;
    NSOperationQueue *queue;
    
    httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    [httpClient setParameterEncoding:AFFormURLParameterEncoding];
    
//    TRACE(@"%@", httpClient);
    
    request = [httpClient requestWithMethod:@"POST"
                                       path:nil
                                 parameters:requestParam];
//    TRACE(@"%@", request);
    
    operation = [[AFJSONRequestOperation alloc] initWithRequest:request];
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    
    switch (status) {
        case NotReachable:{
            TRACE(@"[%d]インターネット接続出来ません", (int)status);
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [IPUtility connectionAlertShow];
        }
            break;
            
        case ReachableViaWWAN:
            TRACE(@"[%d]3G接続 Download", (int)status);
            [operation setCompletionBlockWithSuccess:success failure:failure];
            queue = [[NSOperationQueue alloc] init];
            [queue addOperation:operation];
            break;
            
        case ReachableViaWiFi:{
            TRACE(@"[%d]WiFi接続 Download", (int)status);
            [operation setCompletionBlockWithSuccess:success failure:failure];
            queue = [[NSOperationQueue alloc] init];
            [queue addOperation:operation];
        }
            break;
            
        default:{
            TRACE(@"[%d]default", (int)status);
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [IPUtility connectionAlertShow];
        }
            break;
    }
}


+ (void)loadImageWithURL:(NSURL *)url
                 success:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image))success
                 failure:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error))failure
{
    TRACE(@"url = %@", url);
    Reachability *reachablity = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachablity currentReachabilityStatus];
    
    AFImageRequestOperation *operation;
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
        switch (status) {
            case NotReachable:
                TRACE(@"インターネット接続出来ません");
                break;
                
            case ReachableViaWWAN:
            {
                TRACE(@"3G接続 Download");
                operation = [AFImageRequestOperation imageRequestOperationWithRequest:[NSURLRequest requestWithURL:url]
                                                                 imageProcessingBlock:nil
                                                                              success:success
                                                                              failure:failure];
                [queue addOperation:operation];
            }
                break;
            case ReachableViaWiFi:
            {
                TRACE(@"WiFi接続 Download");
                operation = [AFImageRequestOperation imageRequestOperationWithRequest:[NSURLRequest requestWithURL:url]
                                                                 imageProcessingBlock:nil
                                                                              success:success
                                                                              failure:failure];
                [queue addOperation:operation];
                TRACE(@"");
            }
                break;
            default:
                TRACE(@"？？[%d]", (int)status);
                break;
    }
}

@end
