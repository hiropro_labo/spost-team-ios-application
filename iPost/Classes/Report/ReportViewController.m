//
//  ReportViewController.m
//  iPost.mock.v3
//
//  Created by 北川 義隆 on 2014/04/15.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "ReportViewController.h"

@interface ReportViewController () {
}

@end

@implementation ReportViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    TRACE(@"");
    
    [self setupView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupView
{
    [_btnbgView setBackgroundColor:COLOR_BASS];
    [_viewSeparator setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"dot_h.png"]]];
    [_viewSeparator2 setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"dot_h.png"]]];
}

#pragma mark - UIButton Action
- (IBAction)actionBtnReport:(id)sender
{
}

@end
