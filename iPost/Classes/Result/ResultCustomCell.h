//
//  ResultCustomCell.h
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2013/12/31.
//  Copyright (c) 2013年 北川 義隆. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultCustomCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *homeAndAway;
@property (nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *teamScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *enemyScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *teamLabel;
@property (weak, nonatomic) IBOutlet UILabel *enemyLabel;

@end
