//
//  ResultCustomCell.m
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2013/12/31.
//  Copyright (c) 2013年 北川 義隆. All rights reserved.
//

#import "ResultCustomCell.h"

@implementation ResultCustomCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    self.imageView.frame = CGRectMake(0, 0, self.imageView.image.size.width, self.imageView.image.size.height);
    self.imageView.layer.masksToBounds = YES;
}

@end
