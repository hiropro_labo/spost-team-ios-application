//
//  MenuViewController.m
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2013/10/26.
//  Copyright (c) 2013年 北川 義隆. All rights reserved.
//

#import "ResultViewController.h"
#import "IPResultModel.h"
#import "AFNetworking.h"
#import "ResultCustomCell.h"


#define MENU_CUSTOMCELL @"ResultCustomCell"


@interface ResultViewController () {
    IBOutlet UIImageView *_imageView;
    IBOutlet UITableView *_tableView;
    
    IPResultModel *_model;
    NSString *_parent; // 親データのカテゴリID
    NSUInteger *_index; // テーブルID
}
@end


@implementation ResultViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {}
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    TRACE(@"");
    
    self.navigationItem.title = NAVIGATION_TITLE;
    
    // Result Model の読み込み
    if ( ! _model || _model == nil) {
        _model = [[IPResultModel alloc] init];
    }
    
    [_model download:YES block:nil];
    
    [self jsonLoad];
    
    _tableView.separatorColor = [UIColor blackColor];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    TRACE(@"");
    
    BOOL isCheck = YES;
    if (0 == [_model numberOfData]) {
        isCheck = NO;
    }
    
    [self menuImgDownload:isCheck];
    
    [_tableView registerNib:[UINib nibWithNibName:@"ResultCustomCell" bundle:nil] forCellReuseIdentifier:MENU_CUSTOMCELL];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Json data source

/**
 * NSUserDefaults に保存している JSON 情報を取得する
 */
- (void)jsonLoad
{
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = @"Loading...";
    
    [hud showAnimated:YES whileExecutingBlock:^{
        menuList = [_model loadList];
        resultArray = [[NSMutableArray alloc] init];
        
        for (NSDictionary *obj in menuList) {
            if (obj != nil)
            {
                [resultArray addObject:obj];
            }
        }
        
    } completionBlock:^{
        [hud removeFromSuperview];
        
        [_tableView reloadData];
    }];
}


#pragma mark - Table view data source

/**
 * セルの数
 */
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [resultArray count];
}


/**
 * セルの高さ
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 130;
}


/**
 * テーブルのセル表示処理
 */
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ResultCustomCell *cell = [_tableView dequeueReusableCellWithIdentifier:MENU_CUSTOMCELL forIndexPath:indexPath];
    
    //表示だけ
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    // テキストを一旦保存
    NSString *schedule = [[resultArray objectAtIndex:indexPath.row] objectForKey:JSON_KEY_RESULT_LIST_SCHEDULE];
    
    // タイトルラベルの設定（試合日付）
    cell.labelTitle.text = [NSString stringWithFormat:@" %@", schedule];

    
    //HOME AND AWAY 画像
    NSString *homAndAway = [[resultArray objectAtIndex:indexPath.row] objectForKey:JSON_KEY_RESULT_LIST_HOMEANDAWAY];

    if([homAndAway intValue] == 1){
        cell.homeAndAway.image = [UIImage imageNamed:@"icon_home.png"];
    }
    else{
        cell.homeAndAway.image = [UIImage imageNamed:@"icon_away.png"];
    }
    
    //勝敗画像
    NSString *winOrLose = [[resultArray objectAtIndex:indexPath.row] objectForKey:JSON_KEY_RESULT_LIST_WINORLOSE];

    //背景画像　勝敗で色の設定
    UIImage *winOrLoseImage = [[UIImage alloc] init];
    UIImageView *winOrLoseImageView = [[UIImageView alloc] init];
    if([winOrLose intValue] == 1){
        winOrLoseImage = [UIImage imageNamed:@"background_win_left.png"];
        cell.teamLabel.textColor = [UIColor yellowColor];
        cell.teamScoreLabel.textColor = [UIColor yellowColor];
        cell.enemyLabel.textColor = [UIColor whiteColor];
        cell.enemyScoreLabel.textColor = [UIColor whiteColor];
    }
    else{
        winOrLoseImage = [UIImage imageNamed:@"background_win_right.png"];
        cell.enemyLabel.textColor = [UIColor yellowColor];
        cell.enemyScoreLabel.textColor = [UIColor yellowColor];
        cell.teamLabel.textColor = [UIColor whiteColor];
        cell.teamScoreLabel.textColor = [UIColor whiteColor];
    }

    winOrLoseImageView.image = winOrLoseImage;
    cell.backgroundView = winOrLoseImageView;
    
    
    //スコア
    cell.teamScoreLabel.text = [NSString stringWithFormat:@"%@", [[resultArray objectAtIndex:indexPath.row] objectForKey:JSON_KEY_RESULT_LIST_TEAMSCORE]];
    
    cell.enemyScoreLabel.text = [NSString stringWithFormat:@"%@", [[resultArray objectAtIndex:indexPath.row] objectForKey:JSON_KEY_RESULT_LIST_ENEMYSCORE]];
    
    //チーム名、相手名
    cell.teamLabel.text = [NSString stringWithFormat:@"%@", [[resultArray objectAtIndex:indexPath.row] objectForKey:JSON_KEY_RESULT_LIST_TEAMNAME]];

    cell.enemyLabel.text = [NSString stringWithFormat:@"%@", [[resultArray objectAtIndex:indexPath.row] objectForKey:JSON_KEY_RESULT_LIST_ENEMYNAME]];

    /*
    // 画像ダウンロード
    NSString *imgURL = URL_IMG_MENU_CATE;
    if (_parent != nil || [_parent length] != 0) {
        imgURL = URL_IMG_MENU_ITEM;
    }
    NSString *strURL = [NSString stringWithFormat:@"%@%@",
                        imgURL,
                        [[resultArray objectAtIndex:indexPath.row] objectForKey:JSON_KEY_RESULT_LIST_FILE]];
    NSURL *urlImage = [NSURL URLWithString:strURL];
    
    [cell.imageView setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:IMAGE_PLACEHOLDERIMAGE_MENU2]];
    
    CGSize resize = cell.imageView.bounds.size;
    [cell.imageView setImage: [ClipImage resize:cell.imageView.image resize:resize]];
    cell.imageView.clipsToBounds = YES;
*/
    return cell;
}

#pragma mark - Table view delegate

/**
 * セルの選択
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TRACE(@"TABLE CEL SELECT : %ld", (long)indexPath.row);
    /*
    UIBarButtonItem *back_button = [[UIBarButtonItem alloc] initWithTitle:@"戻る" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = back_button;
    
    [self performSegueWithIdentifier:@"resultDetail" sender:self];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
     */
}


#pragma mark - private method

/*
 * サーバからデータ取得するようモデルに要求
 */
- (void)menuImgDownload:(BOOL)check
{
    TRACE(@"");
    
    NSDictionary *dic = [_model loadTop];
    
    NSString *strURL;
    strURL = [NSString stringWithFormat:@"%@%@",
                URL_IMG_RESULT_TOP,
                [dic objectForKey:JSON_KEY_RESULT_TOP_FILE]];
    NSURL *urlImage = [NSURL URLWithString:strURL];

    TRACE(@"urlImage = %@", urlImage);

    [_imageView setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:IMAGE_PLACEHOLDERIMAGE_MENU1]];
    [_tableView reloadData];
}

- (NSDictionary *)getCurrData
{
    return [resultArray objectAtIndex:[_tableView indexPathForSelectedRow].row];
}

@end