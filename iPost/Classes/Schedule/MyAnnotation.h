//
//  MyAnnotation.h
//  sPost.team.mock.v1
//
//  Created by ヒロ企画 on 2014/05/14.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MyAnnotation : NSObject <MKAnnotation> {
    CLLocationCoordinate2D coordinate;
    NSString *annotationTitle;
    NSString *annotationSubtitle;
}

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, strong) NSString *annotationTitle;
@property (nonatomic, strong) NSString *annotationSubtitle;
- (id)initWithLocationCoordinate:(CLLocationCoordinate2D)_coordinate
                           title:(NSString *)_annotationTitle
                        subtitle:(NSString *)_annotationannSubtitle;
- (NSString *)title;
- (NSString *)subtitle;

@end