//
//  MyAnnotation.m
//  sPost.team.mock.v1
//
//  Created by ヒロ企画 on 2014/05/14.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "MyAnnotation.h"

@implementation MyAnnotation
@synthesize coordinate;
@synthesize annotationTitle;
@synthesize annotationSubtitle;

- (NSString *)title {
    return annotationTitle;
}

- (NSString *)subtitle {
    return annotationSubtitle;
}

- (id)initWithLocationCoordinate:(CLLocationCoordinate2D)_coordinate
                           title:(NSString *)_annotationTitle
                        subtitle:(NSString *)_annotationSubtitle {
    coordinate = _coordinate;
    self.annotationTitle = _annotationTitle;
    self.annotationSubtitle = _annotationSubtitle;
    return self;
}

@end
