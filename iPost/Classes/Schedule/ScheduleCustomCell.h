//
//  ScheduleCustomCell.h
//  sPost-Mock1
//
//  Created by 北川 義隆 on 2013/12/31.
//  Copyright (c) 2013年 北川 義隆. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScheduleCustomCell : UITableViewCell

@property (nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIImageView *homeAndAway;
@property (weak, nonatomic) IBOutlet UIImageView *scheduleImageView;

@end
