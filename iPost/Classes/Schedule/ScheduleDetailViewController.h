//
//  ScheduleDetailViewController.h
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2013/10/28.
//  Copyright (c) 2013年 北川 義隆. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IPScheduleModel.h"

#define COLOR_DETAIL     RGB(50, 50, 50)
#define DETAIL_FONTSIZE  13.
#define DETAIL_WIDTH     280.

@interface ScheduleDetailViewController : UIViewController <OHAttributedLabelDelegate>

- (void)setData:(NSDictionary *)dicData;
- (NSDictionary *)getData;
- (NSURL *)getUrl;
- (void)setUrl:(NSURL *)str;
@end
