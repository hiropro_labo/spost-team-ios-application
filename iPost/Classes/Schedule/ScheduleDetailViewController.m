//
//  ScheduleDetailViewController.m
//  sPost-Mock1
//
//  Created by 北川 義隆 on 2013/10/28.
//  Copyright (c) 2014年 ヒロ企画. All rights reserved.
//

#import "ScheduleDetailViewController.h"
#import "MyAnnotation.h"
#import "AFNetworking.h"
#import "ClipImage.h"
#import <QuartzCore/QuartzCore.h>


@interface ScheduleDetailViewController () <OHAttributedLabelDelegate>
{
    IBOutlet UIScrollView *_scrollView;
    IBOutlet UIImageView *_imageView;   // Top画像
    IBOutlet UIView *_itemView;         // 対戦View土台
    __weak IBOutlet UILabel *titleLabel;
    __weak IBOutlet UIButton *routeButton;
    IBOutlet MKMapView *mapView;
    __weak IBOutlet UIImageView *scheduleImageView;
    __weak IBOutlet UILabel *placeLabel;
    __weak IBOutlet UILabel *playTimeLabel;
    __weak IBOutlet UILabel *lCodeLabel;
    
    IPScheduleModel *_model;
    NSDictionary *_menu;
    
    NSURL *_url;
}
@end


@implementation ScheduleDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {}
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = NAVIGATION_TITLE;
    
    //地図アプリ表示ボタン
    routeButton.layer.borderWidth = 1;
    routeButton.layer.borderColor = RGB(94, 46, 142).CGColor;
    routeButton.layer.cornerRadius = 2;
    
    _menu = [_model getDataAtIndex:0];

    [self setupMap];
    
    UIBarButtonItem *backBarButtonItem =
    [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:BACK_BUTTON_IMG]
                                     style:UIBarButtonItemStylePlain
                                    target:self.navigationController
     
                                    action:@selector(popViewControllerAnimated:)];
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    TRACE(@"");

    /// Top画像
    NSDictionary *dic = [_model loadTop];
    
    NSString *strURL;
    strURL = [NSString stringWithFormat:@"%@%@",
              URL_IMG_SCHEDULE_TOP,
              [dic objectForKey:JSON_KEY_SCHEDULE_TOP_FILE]];
    
    NSURL *urlImage = [NSURL URLWithString:strURL];
    TRACE(@"urlImage = %@", urlImage);
    [_imageView setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:IMAGE_PLACEHOLDERIMAGE_MENU_DETAIL]];
    TRACE(@"image w = %f, h = %f",_imageView.image.size.width, _imageView.image.size.height);
    
    // タイトル用,対戦日付
    titleLabel.text = [NSString stringWithFormat:@" %@", [_menu objectForKey:JSON_KEY_SCHEDULE_LIST_SCHEDULE]];
  
    //対戦画像
    NSString *imageFileName = [NSString stringWithFormat:@"team%@.png", [_menu objectForKey:@"enemy"]];
    [scheduleImageView setImage:[UIImage imageNamed:imageFileName]];

    //会場ラベル
    placeLabel.text = [_menu objectForKey:JSON_KEY_SCHEDULE_LIST_PLACE];
    
    //プレイタイム
    playTimeLabel.text = [_menu objectForKey:JSON_KEY_SCHEDULE_LIST_PLAYTIME];
    
    //Lコード戦
    lCodeLabel.text = [_menu objectForKey:JSON_KEY_SCHEDULE_LIST_LCODE];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    TRACE(@"");
    
    //ScrollViewスクロールサイズ(height)
    CGSize size = _scrollView.contentSize;
    size.height = 1050;
    _scrollView.contentSize = size;
    
    //Top画像サイズ
    CGSize resize = _imageView.bounds.size;
    [_imageView setImage: [ClipImage resize:_imageView.image resize:resize]];
    _imageView.clipsToBounds = YES;
    
    [titleLabel setBackgroundColor:COLOR_BASS];

    [self.view layoutSubviews];
}

/**
 * 全角判断
 * @return bool YES:半角/NO:全角含む
 */
- (BOOL)isAllHalfWidthCharacter:(NSString *)num
{
    NSUInteger nsStringlen = [num length];
    const char *utf8 = [num UTF8String];
    size_t cStringlen = strlen(utf8);
    if (nsStringlen == cStringlen) {
        return YES;
    } else {
        return NO;
    }
}

#pragma mark - public method

- (void)setData:(NSDictionary *)dicData
{
    if (!_model || _model == nil) {
        _model = [[IPScheduleModel alloc] init];
    }
    [_model setData:dicData];
}

- (NSDictionary *)getData
{
    NSDictionary *dic = [_model getDataAtIndex:0];
    
    return dic;
}

- (NSURL *)getUrl
{
    return _url;
}

#pragma mark - private method
- (void)setUrl:(NSURL *)str
{
    _url = str;
}

/**
 地図の設定
 **/
- (void)setupMap
{
    // ピンの位置,中心点
    NSString *strLat = [_menu objectForKey:JSON_KEY_SCHEDULE_PLACE_LAT];
    NSString *strLng = [_menu objectForKey:JSON_KEY_SCHEDULE_PLACE_LNG];
    CLLocationCoordinate2D appleStoreGinzaCoord;
    appleStoreGinzaCoord.latitude = [strLat floatValue]; // 経度
    appleStoreGinzaCoord.longitude = [strLng floatValue]; // 緯度
    [mapView setCenterCoordinate:appleStoreGinzaCoord animated:NO];
    
    // 縮尺
    MKCoordinateRegion mapScale = mapView.region;
    mapScale.center = appleStoreGinzaCoord;
    mapScale.span.latitudeDelta = mapScale.span.longitudeDelta = 0.004;
    [mapView setRegion:mapScale animated:NO];
    
    // annotation の追加
    NSArray *annotations = @[
                             [[MyAnnotation alloc] initWithLocationCoordinate:appleStoreGinzaCoord
                                                                        title:nil
                                                                     subtitle:nil]
                             ];
    [mapView addAnnotations:annotations];
}
/*
 地図表示のカスタム
 */
/*
-(MKAnnotationView*)mapView:(MKMapView*)mapView viewForAnnotation:(id)annotation{
    
    static NSString *PinIdentifier = @"Pin";
    MKPinAnnotationView *pin = (MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:PinIdentifier];
    if (pin == nil){
        pin = [[MKPinAnnotationView alloc]
               initWithAnnotation:annotation
               reuseIdentifier:PinIdentifier];
        pin.animatesDrop = YES;  // アニメーションをする
        pin.pinColor = MKPinAnnotationColorPurple;  // ピンの色を紫にする
        pin.canShowCallout = YES;  // ピンをタップするとコールアウトを表示
    }
    return pin;
}
*/
//地図アプリ呼び出し
-(IBAction)mapButton:(id)sender
{
    TRACE(@"mapButton");
    NSString *strLat = [_menu objectForKey:JSON_KEY_SCHEDULE_PLACE_LAT];
    NSString *strLng = [_menu objectForKey:JSON_KEY_SCHEDULE_PLACE_LNG];
    
    UIAlertView *alert;
    if([strLat length] < 1 || [strLng length] < 1) {
        alert = [[UIAlertView alloc] initWithTitle:nil
                                           message:@"マップの情報がありません。"
                                          delegate:self
                                 cancelButtonTitle:@"閉じる"
                                 otherButtonTitles:nil, nil];
    } else {
        alert = [[UIAlertView alloc] initWithTitle:nil
                                           message:@"マップへ移動します。"
                                          delegate:self
                                 cancelButtonTitle:@"キャンセル"
                                 otherButtonTitles:@"OK", nil];
    }
    [alert show];
}

/*
 * 試合会場の位置を地図上に表示
 */
 - (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
 {
     if (buttonIndex == alertView.firstOtherButtonIndex) {
         NSString *strLat = [_menu objectForKey:JSON_KEY_SCHEDULE_PLACE_LAT];
         NSString *strLng = [_menu objectForKey:JSON_KEY_SCHEDULE_PLACE_LNG];
         
         double lat = strLat.doubleValue;
         double lng = strLng.doubleValue;
 
         CLLocationCoordinate2D loc;
         loc.latitude  = lat;
         loc.longitude = lng;
         NSDictionary *addressDic = [NSDictionary dictionaryWithObject:[_menu objectForKey:JSON_KEY_SCHEDULE_LIST_PLACE]
                                                                forKey:(NSString *)kABPersonAddressCityKey];
         MKPlacemark *placeMark = [[MKPlacemark alloc] initWithCoordinate:loc addressDictionary:addressDic];
         MKMapItem *map = [[MKMapItem alloc] initWithPlacemark:placeMark];
         NSDictionary *launchDic = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:MKMapTypeStandard]
                                                               forKey:MKLaunchOptionsMapTypeKey];
         [map openInMapsWithLaunchOptions:launchDic];
     }
 }
 
@end
