//
//  ScheduleViewController.h
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2013/10/26.
//  Copyright (c) 2013年 北川 義隆. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ScheduleViewController : UIViewController
{
    NSArray *menuList;
    NSMutableArray *menuArray;
}

//- (void)setParent:(NSString *)parent;
//- (void)setIndex:(NSUInteger *)index;
- (NSDictionary *)getCurrData;

@end
