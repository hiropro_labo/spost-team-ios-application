//
//  MenuViewController.m
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2013/10/26.
//  Copyright (c) 2013年 北川 義隆. All rights reserved.
//

#import "ScheduleViewController.h"
#import "IPScheduleModel.h"
#import "AFNetworking.h"
#import "ScheduleCustomCell.h"
#import "ClipImage.h"

#define MENU_CUSTOMCELL @"ScheduleCustomCell"


@interface ScheduleViewController () {
    
    IBOutlet UIImageView *_imageView;
    IBOutlet UITableView *_tableView;
    
    IPScheduleModel *_model;
}
@end


@implementation ScheduleViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {}
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    TRACE(@"");
    
//    _parent = @"139";
//    _index = (NSUInteger *)2;
    
    self.navigationItem.title = NAVIGATION_TITLE;

    // News Model の読み込み
    if ( ! _model || _model == nil) {
        _model = [[IPScheduleModel alloc] init];
    }
    
    [_model download:YES block:nil];
    
    [self jsonLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    TRACE(@"");
    
    BOOL isCheck = YES;
    if (0 == [_model numberOfData]) {
        isCheck = NO;
    }
    
    [self menuImgDownload:isCheck];
    
    [_tableView registerNib:[UINib nibWithNibName:@"ScheduleCustomCell" bundle:nil] forCellReuseIdentifier:MENU_CUSTOMCELL];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Json data source

/**
 * NSUserDefaults に保存している JSON 情報を取得する
 */
- (void)jsonLoad
{
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = @"Loading...";
    
    [hud showAnimated:YES whileExecutingBlock:^{
        menuList = [_model loadList];
        menuArray = [[NSMutableArray alloc] init];
        
        for (NSDictionary *obj in menuList) {
            if (obj != nil)
            {
                [menuArray addObject:obj];
            }
        }
        
    } completionBlock:^{
        [hud removeFromSuperview];
        
        [_tableView reloadData];
    }];
}


#pragma mark - Table view data source

/**
 * セルの数
 */
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [menuArray count];
}


/**
 * セルの高さ
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 160.;
}


/**
 * テーブルのセル表示処理
 */
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ScheduleCustomCell *cell = [_tableView dequeueReusableCellWithIdentifier:MENU_CUSTOMCELL forIndexPath:indexPath];
    
    // テキストを一旦保存
    NSString *schedule = [[menuArray objectAtIndex:indexPath.row] objectForKey:JSON_KEY_SCHEDULE_LIST_SCHEDULE];
    NSString *homAndAway = [[menuArray objectAtIndex:indexPath.row] objectForKey:JSON_KEY_SCHEDULE_LIST_HOMEANDAWAY];
    
    // タイトルラベルの設定（試合日付）
    cell.labelTitle.text = [NSString stringWithFormat:@" %@", schedule];
    [cell.labelTitle setBackgroundColor:COLOR_BASS];

    //HOME AND AWAY 画像
    if([homAndAway intValue] == 1){
        cell.homeAndAway.image = [UIImage imageNamed:@"icon_home.png"];
    }
    else{
        cell.homeAndAway.image = [UIImage imageNamed:@"icon_away.png"];
    }

    //対戦画像ファイル名
    NSString *fineName = [NSString stringWithFormat:@"team%@.png", [menuArray objectAtIndex:indexPath.row][JSON_KEY_SCHEDULE_LIST_ENEMY]];
                          
    [cell.scheduleImageView setImage:[UIImage imageNamed:fineName]];

    //アクセサリー画像
    UIImage *arrowImage = [UIImage imageNamed:@"arrow.png"];
    UIImageView *arrowImageView = [[UIImageView alloc] initWithImage:arrowImage];
    
    //画像が大きい場合にはみ出さないようにViewの大きさを固定化
    arrowImageView.frame = CGRectMake(0, 0, 18, 27);
    //アクセサリービューにイメージを設定
    cell.accessoryView = arrowImageView;
    
    
    return cell;

}


#pragma mark - Table view delegate

/**
 * セルの選択 スケジュール詳細へ
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TRACE(@"TABLE CEL SELECT : %ld", (long)indexPath.row);
    
    UIBarButtonItem *back_button = [[UIBarButtonItem alloc] initWithTitle:nil style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = back_button;

    [self performSegueWithIdentifier:@"scheduleDetail" sender:self];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}


#pragma mark - private method

/*
 * サーバからデータ取得するようモデルに要求
 */
- (void)menuImgDownload:(BOOL)check
{
    TRACE(@"");
    
    NSDictionary *dic = [_model loadTop];
    
    NSString *strURL;
        strURL = [NSString stringWithFormat:@"%@%@",
                URL_IMG_SCHEDULE_TOP,
                [dic objectForKey:JSON_KEY_SCHEDULE_TOP_FILE]];
    NSURL *urlImage = [NSURL URLWithString:strURL];

    TRACE(@"urlImage = %@", urlImage);

    [_imageView setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:IMAGE_PLACEHOLDERIMAGE_MENU1]];

    [_tableView reloadData];
}

- (NSDictionary *)getCurrData
{
    return [menuArray objectAtIndex:[_tableView indexPathForSelectedRow].row];
}

@end