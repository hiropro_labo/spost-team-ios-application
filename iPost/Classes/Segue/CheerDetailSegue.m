//
//  MenuDetailSegue.m
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2013/10/28.
//  Copyright (c) 2013年 北川 義隆. All rights reserved.
//

#import "CheerDetailSegue.h"
#import "CheerViewController.h"
#import "CheerDetailViewController.h"


@implementation CheerDetailSegue

- (void)perform
{
    CheerViewController *srcViewController = (CheerViewController *)self.sourceViewController; //遷移元
    CheerDetailViewController *destViewController = (CheerDetailViewController *)self.destinationViewController; //遷移先
    
    NSDictionary *dic = [srcViewController getCurrData];
//    TRACE(@"dic = %@", dic);
    [destViewController setData:dic];
    [srcViewController.navigationController pushViewController:destViewController animated:YES];
}

@end
