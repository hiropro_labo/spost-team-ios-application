//
//  MainAcademyWebSegue.m
//  iPost.81
//
//  Created by ヒロ企画 on 2014/04/10.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "MainAcademyWebSegue.h"
#import "MainViewController.h"
#import "WebViewController.h"

@implementation MainAcademyWebSegue

- (void)perform
{
    MainViewController *srcViewController = (MainViewController *)self.sourceViewController; //遷移元
    WebViewController *destViewController = (WebViewController *)self.destinationViewController; //遷移先
    
    // URLデータ有無チェック
#if 0
#warning 仮URL
    NSString *strUrl = @"http://www.google.co.jp";
#else
    NSString *strUrl = @"http://www.fivearrows.jp/academy/";
    TRACE(@"strUrl = %@", strUrl);
#endif
    
    [destViewController setURL:[NSURL URLWithString:strUrl]];
    destViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [srcViewController presentViewController:destViewController animated:YES completion:nil];
}

@end
