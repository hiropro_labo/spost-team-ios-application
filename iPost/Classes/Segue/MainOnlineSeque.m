//
//  MainOnlineSeque.m
//  iPost.mock.v3
//
//  Created by 北川 義隆 on 2014/02/27.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "MainOnlineSeque.h"
#import "MainViewController.h"
#import "WebViewController.h"
#import "IPMainModel.h"

@implementation MainOnlineSeque

- (void)perform
{
    MainViewController *srcViewController = (MainViewController *)self.sourceViewController; //遷移元
    WebViewController *destViewController = (WebViewController *)self.destinationViewController; //遷移先
    
    // URLデータ有無チェック
#if 0
#warning 仮URL
    NSString *strUrl = @"http://www.google.co.jp";
#else
    NSDictionary *dic = [srcViewController getClientInfo];
    NSString *strUrl = [dic objectForKey:JSON_KEY_MAIN_SHOP_ONLINE];
    TRACE(@"strUrl = %@", strUrl);
#endif
    if(strUrl == nil || [strUrl isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"オンラインショップの情報がありません。"
                                                       delegate:self
                                              cancelButtonTitle:@"閉じる"
                                              otherButtonTitles:nil, nil];
        [alert show];
    } else {
        [destViewController setURL:[NSURL URLWithString:strUrl]];
        destViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        [srcViewController presentViewController:destViewController animated:YES completion:nil];
    }
}

@end
