//
//  MainShopSegue.m
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2014/01/05.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "MainShopSegue.h"
#import "MainViewController.h"
#import "ShopDetailViewController.h"

@implementation MainShopSegue

- (void)perform
{
    MainViewController *srcViewController = (MainViewController *)  self.sourceViewController; //遷移元
    ShopDetailViewController *destViewController = (ShopDetailViewController *) self.destinationViewController; //遷移先
    
    [destViewController setClientInfo:[srcViewController getClientInfo]];
    [srcViewController.navigationController pushViewController:destViewController animated:YES];
}

@end
