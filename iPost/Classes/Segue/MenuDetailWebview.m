//
//  MenuDetailWebview.m
//  iPost.mock.v3
//
//  Created by 北川 義隆 on 2014/03/25.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "MenuDetailWebview.h"
#import "TeamDetailViewController.h"
#import "WebViewController.h"
#import "IPScheduleModel.h"

@implementation MenuDetailWebview

- (void)perform
{
    TeamDetailViewController *srcViewController = (TeamDetailViewController *)self.sourceViewController; //遷移元
    WebViewController *destViewController = (WebViewController *)self.destinationViewController; //遷移先
    // URLデータ有無チェック
    NSURL *strUrl = [srcViewController getUrl];
    TRACE(@"strUrl = %@", strUrl);
    if(strUrl == nil || [[strUrl absoluteString] isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"情報がありません。"
                                                       delegate:self
                                              cancelButtonTitle:@"閉じる"
                                              otherButtonTitles:nil, nil];
        [alert show];
    } else {
        [destViewController setURL:strUrl];
        destViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        [srcViewController presentViewController:destViewController animated:YES completion:nil];
}
}

@end
