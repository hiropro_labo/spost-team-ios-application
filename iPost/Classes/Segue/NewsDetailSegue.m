//
//  NewsDetailSegue.m
//  iPost.81
//
//  Created by ヒロ企画 on 2014/04/11.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "NewsDetailSegue.h"
#import "MainViewController.h"
#import "NewsDetailViewController.h"

@implementation NewsDetailSegue

- (void)perform
{
    MainViewController *srcViewController = (MainViewController *)self.sourceViewController; //遷移元
    NewsDetailViewController *destViewController = (NewsDetailViewController *)self.destinationViewController; //遷移先
    
    NSDictionary *dic = [srcViewController getCurrData];
    //    TRACE(@"dic = %@", dic);
    [destViewController setData:dic];
    
/*
    [UIView transitionWithView:srcViewController.navigationController.view
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [srcViewController.navigationController pushViewController:destViewController animated:NO];
                    }
                    completion:nil];
*/
    
    [srcViewController.navigationController pushViewController:destViewController animated:YES];
}

@end
