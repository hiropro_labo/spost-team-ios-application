//
//  ScheduleDetailSegue.m
//  iPost.81
//
//  Created by ヒロ企画 on 2014/04/11.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "ScheduleDetailSegue.h"
#import "ScheduleViewController.h"
#import "ScheduleDetailViewController.h"

@implementation ScheduleDetailSegue

- (void)perform
{
    ScheduleViewController *srcViewController = (ScheduleViewController *)self.sourceViewController; //遷移元
    ScheduleDetailViewController *destViewController = (ScheduleDetailViewController *)self.destinationViewController; //遷移先
    
    NSDictionary *dic = [srcViewController getCurrData];
    //    TRACE(@"dic = %@", dic);
    [destViewController setData:dic];
    [srcViewController.navigationController pushViewController:destViewController animated:YES];
}

@end
