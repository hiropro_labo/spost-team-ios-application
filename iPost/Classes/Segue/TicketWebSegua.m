//
//  TicketWebSegua.m
//  sPost.team.mock.v1
//
//  Created by ヒロ企画 on 2014/05/20.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "TicketWebSegua.h"
#import "TicketViewController.h"
#import "WebViewController.h"

@implementation TicketWebSegua

- (void)perform
{
    TicketViewController *srcViewController = (TicketViewController *)self.sourceViewController; //遷移元
    WebViewController *destViewController = (WebViewController *)self.destinationViewController; //遷移先
    
    // URLデータ有無チェック
#if 0
#warning 仮URL
    NSString *strUrl = @"http://www.google.co.jp";
#else
    NSString *strUrl = @"http://tokyo-cinqreves.jp/ticket/normal/";
    TRACE(@"strUrl = %@", strUrl);
#endif
    
    [destViewController setURL:[NSURL URLWithString:strUrl]];
    destViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [srcViewController presentViewController:destViewController animated:YES completion:nil];
}

@end
