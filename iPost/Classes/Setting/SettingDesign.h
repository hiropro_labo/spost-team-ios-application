//
//  SettingDesign.h
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2014/01/01.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SETTING_DESIGN_ROWTYPE          @"rowtype"
#define SETTING_DESIGN_TEXT             @"text"
#define SETTING_DESIGN_SWITCH           @"switch"
#define SETTING_DESIGN_ACCESSORY        @"accessory"
#define SETTING_DESIGN_SELECTIONSTYLE   @"selection"
#define SETTING_DESIGN_URL              @"url"


typedef enum : NSInteger {
    SETTING_DESIGN_ROWTYPE_NOTIFICATION = 0,
    SETTING_DESIGN_ROWTYPE_SHOPNAME,
    SETTING_DESIGN_ROWTYPE_MAIL,
    SETTING_DESIGN_ROWTYPE_APPHOWTO,
    SETTING_DESIGN_ROWTYPE_MAX
} settingRowType;


@interface SettingDesign : NSObject

- (NSInteger)numberOfDesign;
- (void)makeDesign;
- (NSDictionary *)getDesignAtIndex:(NSInteger)index;

@end
