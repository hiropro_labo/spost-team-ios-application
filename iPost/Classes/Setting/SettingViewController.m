//
//  SettingViewController.m
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2014/01/01.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "SettingViewController.h"
#import "IPSettingModel.h"
#import "SettingCustomCell.h"
#import "SettingDesign.h"
#import "AppHowtoViewController.h"

#define SETTING_CUSTOMCELL @"settingCustomCell"

@interface SettingViewController () {
    IPSettingModel *_model;
    SettingDesign *_designData;
    NSArray *_array;
    NSDictionary *_json;
    NSString *_currEmail;
}
@end

@implementation SettingViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    TRACE(@"");
    
    self.navigationItem.title = NAVIGATION_TITLE;
    
    if (!_model || _model == nil) {
        _model = [[IPSettingModel alloc] init];
    }
    
    if ([_model load:DEF_SETTING_JSON] == nil) {
        [_model download:YES block:nil];
    }
    
    _array = (NSArray *)[_model load:DEF_SETTING_JSON];
    _json = [_array objectAtIndex:0];
    TRACE(@"%@", _json);
    
    if (!_designData || _designData == nil) {
        _designData = [[SettingDesign alloc] init];
        TRACE(@"_designData = %@", _designData);
    }
    [_designData makeDesign];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"SettingCustomCell" bundle:nil] forCellReuseIdentifier:SETTING_CUSTOMCELL];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_designData numberOfDesign];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = [_designData getDesignAtIndex:indexPath.section];
    
    if (indexPath.section == SETTING_DESIGN_ROWTYPE_MAIL) {
        if ([dic[SETTING_DESIGN_URL] isEqualToString:@""]) {
            return 0.;
        }
    }
    return 44.;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section;
{
    NSString *resString;
    switch (section) {
        case 0:
            resString = @"PUSH通知設定";
            break;
        case 1:
            resString = @"会社情報";
            break;
        case 3:
            resString = @"その他";
            break;
        default:
            resString = nil;
            break;
    }
    
    return resString;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return nil;
}


- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section;
{
    return nil;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TRACE(@"");
    SettingCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:SETTING_CUSTOMCELL forIndexPath:indexPath];
    
    [cell putEffect];
    
    NSDictionary *dic = [_designData getDesignAtIndex:indexPath.section];
    
    cell.labelTitle.text = dic[SETTING_DESIGN_TEXT];
    cell.labelTitle.adjustsFontSizeToFitWidth = YES;
    
    cell.switchInput.hidden = ![dic[SETTING_DESIGN_SWITCH] boolValue];
    cell.accessoryType = [dic[SETTING_DESIGN_ACCESSORY] intValue];
    cell.selectionStyle = [dic[SETTING_DESIGN_SELECTIONSTYLE] intValue];
    
    if (indexPath.section == SETTING_DESIGN_ROWTYPE_SHOPNAME) {
        if ([dic[SETTING_DESIGN_TEXT] isEqualToString:@""]) {
            cell.labelTitle.text = @"未設定";
        }
        
        if ([dic[SETTING_DESIGN_URL] isEqualToString:IPOST_URL]) {
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
    }
    
    if (indexPath.section == SETTING_DESIGN_ROWTYPE_MAIL) {
        if ([dic[SETTING_DESIGN_URL] isEqualToString:@""]) {
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.hidden = YES;
        } else {
            cell.hidden = NO;
        }
    }
    
    TRACE(@"SWITCH INPU SELECTED : %@", [_json objectForKey:JSON_KEY_SETTING_ALLOW_FLG]);
    if ([[_json objectForKey:JSON_KEY_SETTING_ALLOW_FLG] isEqualToString:@"1"]) {
        cell.switchInput.on = YES;
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = [_designData getDesignAtIndex:indexPath.section];
    
    [self sendWebView:dic indexPath:indexPath];
    
    // メーラー
    if (indexPath.section == SETTING_DESIGN_ROWTYPE_MAIL) {
        _currEmail = dic[SETTING_DESIGN_URL];
        TRACE(@"strUrl = %@", _currEmail);
        [self emailCall];
    }

    // このアプリについて
    if (indexPath.section == SETTING_DESIGN_ROWTYPE_APPHOWTO) {
        UIBarButtonItem *back_button = [[UIBarButtonItem alloc] initWithTitle:@"戻る" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.navigationItem.backBarButtonItem = back_button;
        AppHowtoViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"appHowto"];
        [self.navigationController pushViewController:controller animated:YES];
    }
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark - Alert view delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (alertView.tag) {
        case 1:{
            if (buttonIndex == alertView.firstOtherButtonIndex) {
                [IPUtility mailToWithStringAddress:_currEmail];
            }
        }
            break;
        default:
            break;
    }
}

#pragma mark - action method

- (void)actToggle:(id)sender
{
    TRACE(@"");
    
    UISwitch *sw = sender;
    BOOL notif = 0;
    
    if (sw.on) {
        TRACE(@"スイッチがONになりました。");
        notif = 1;
    } else {
        TRACE(@"スイッチがOFFになりました。");
        notif = 0;
    }
    [self sendNotification:notif];
}

#pragma mark - private method

/**
 * WebView表示切り替え
 */
- (void)sendWebView:(NSDictionary *)dic indexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == SETTING_DESIGN_ROWTYPE_SHOPNAME) {
        // 「戻る」に名称変更
        UIBarButtonItem *back_button = [[UIBarButtonItem alloc] initWithTitle:@"戻る" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.navigationItem.backBarButtonItem = back_button;
        
        NSString *strUrl = dic[SETTING_DESIGN_URL];
        TRACE(@"strUrl = %@", strUrl);
        
        if (indexPath.section == SETTING_DESIGN_ROWTYPE_SHOPNAME) {
            if ( ! [strUrl isEqualToString:IPOST_URL]) {
                [self performSegueWithIdentifier:@"settingWebview" sender:self];
            }
        } else {
            [self performSegueWithIdentifier:@"settingHelpWebview" sender:self];
        }
    }
}

- (void)emailCall
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:@"メーラーを立ち上げます。よろしいですか？"
                                                   delegate:self
                                          cancelButtonTitle:@"いいえ"
                                          otherButtonTitles:@"はい", nil];
    alert.tag = 1;
    [alert show];
}

/**
 * 通知設定の情報をサーバーに送る
 */
- (void)sendNotification:(BOOL)notif
{
    TRACE(@"");
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URL_API_NOTIF]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    
    NSString *token = [[NSUserDefaults standardUserDefaults] stringForKey:DEF_TOKEN];
    NSString *data = [NSString stringWithFormat:@"client_id=%@&token=%@&notif=%d", CLIENT_ID, token, notif];
    [request setHTTPBody:[data dataUsingEncoding:NSUTF8StringEncoding]];
    
    TRACE(@"%@ : POST : %@", request, data);
    
    NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];
    if (connection) {
        TRACE(@"Notification SEND : OK");
    } else {
        TRACE(@"Notification SEND : ERROR");
    }
}


#pragma mark - public method

- (NSString *)getUrl
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    NSDictionary *dic = [_designData getDesignAtIndex:indexPath.section];
    
    return dic[SETTING_DESIGN_URL];
}

@end
