//
//  SponsorViewController.m
//  sPost.team.mock.v1
//
//  Created by ヒロ企画 on 2014/05/20.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "SponsorViewController.h"
#import "IPSponsorModel.h"
#import "SponsorCustomCell.h"

@interface SponsorViewController ()
{
    __weak IBOutlet UITableView *_tableView;
    IPSponsorModel *_model;
}

@end

#define MENU_CUSTOMCELL @"SponsorCustomCell"

@implementation SponsorViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    TRACE(@"");
    
    // Sponsor Model の読み込み
    if ( ! _model || _model == nil) {
        _model = [[IPSponsorModel alloc] init];
    }
    
    [_model download:YES block:nil];
    
    [self jsonLoad];
    
    UIImage *backgroundImage = [UIImage imageNamed:@"list_background_image.png"];
    _tableView.backgroundView = [[UIImageView alloc] initWithImage:backgroundImage];
    _tableView.backgroundColor = RGBA(0, 0, 0, 0.99f);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    TRACE(@"");
    
    BOOL isCheck = YES;
    if (0 == [_model numberOfData]) {
        isCheck = NO;
    }
    
//    [self menuImgDownload:isCheck];
    
    [_tableView registerNib:[UINib nibWithNibName:@"SponsorCustomCell" bundle:nil] forCellReuseIdentifier:MENU_CUSTOMCELL];
}

#pragma mark - Json data source

/**
 * NSUserDefaults に保存している JSON 情報を取得する
 */
- (void)jsonLoad
{
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = @"Loading...";
    
    [hud showAnimated:YES whileExecutingBlock:^{
        sponsorArray = (NSMutableArray *)[_model load:DEF_SPONSOR_JSON];
        
    } completionBlock:^{
        [hud removeFromSuperview];
        
        [_tableView reloadData];
    }];
}

#pragma mark - Table view data source

/**
 * セルの数
 */
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [sponsorArray count];
}

/**
 * セルの背景色
 */
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
}

/**
 * セルの高さ
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

/**
 * テーブルのセル表示処理
 */
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SponsorCustomCell *cell = [_tableView dequeueReusableCellWithIdentifier:MENU_CUSTOMCELL forIndexPath:indexPath];
    
    //アプリ名
    cell.appNameLabel.text = [[sponsorArray objectAtIndex:indexPath.row] objectForKey:JSON_KEY_SPONSOR_LIST_APPNAME];
    
    //スポンサー名
    cell.sponsorName.text = [[sponsorArray objectAtIndex:indexPath.row] objectForKey:JSON_KEY_SPONSOR_LIST_SPONSORNAME];
    
    // 画像ダウンロード
    NSString *strURL = [NSString stringWithFormat:@"%@%@/%@",
                        URL_IMG_SPONSOR_ICON, [sponsorArray objectAtIndex:indexPath.row][JSON_KEY_SPONSOR_LIST_SPONSORID],
                        [sponsorArray objectAtIndex:indexPath.row][JSON_KEY_SPONSOR_LIST_FILE]];
    
    NSURL *urlImage = [NSURL URLWithString:strURL];
    
    //画像がずれる症状のとりあえずの処置
    cell.contentView.frame = CGRectMake(0, 0, 0, 0);
    
    [cell.appImageView setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:IMAGE_PLACEHOLDERIMAGE_MENU2]];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}


#pragma mark - Table view delegate

/**
 * セルの選択
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TRACE(@"TABLE CEL SELECT : %ld", (long)indexPath.row);

    NSString *urlString = [[sponsorArray objectAtIndex:indexPath.row] objectForKey:JSON_KEY_SPONSOR_LIST_APPURLIOS];
    
    NSURL* url = [[NSURL alloc] initWithString:urlString];
    
    [[UIApplication sharedApplication] openURL:url];
}


#pragma mark - private method
/*
 * サーバからデータ取得するようモデルに要求
 */
/*
- (void)menuImgDownload:(BOOL)check
{
    TRACE(@"");
    
    NSDictionary *dic = [_model loadTop];
    
    NSString *strURL;
    strURL = [NSString stringWithFormat:@"%@%@",
              URL_IMG_SPONSOR_TOP,
              [dic objectForKey:JSON_KEY_SPONSOR_TOP_FILE]];
    NSURL *urlImage = [NSURL URLWithString:strURL];
    
    TRACE(@"urlImage = %@", urlImage);
    
    [_imageView setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:IMAGE_PLACEHOLDERIMAGE_MENU1]];
    
    [_tableView reloadData];
}
*/
- (NSDictionary *)getCurrData
{
    return [sponsorArray objectAtIndex:[_tableView indexPathForSelectedRow].row];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
