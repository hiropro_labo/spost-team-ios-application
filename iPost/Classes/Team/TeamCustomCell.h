//
//  MenuCustomCell.h
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2013/12/31.
//  Copyright (c) 2013年 北川 義隆. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TeamCustomCell : UITableViewCell

@property (nonatomic, readonly) IBOutlet UIImageView *imageView;

@end
