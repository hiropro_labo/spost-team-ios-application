//
//  MenuDetailViewController.h
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2013/10/28.
//  Copyright (c) 2013年 北川 義隆. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IPTeamModel.h"

#define COLOR_DETAIL     RGB(50, 50, 50)
#define DETAIL_FONTSIZE  13.
#define DETAIL_WIDTH     280.

@interface TeamDetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *birthDayLabel;
@property (weak, nonatomic) IBOutlet UILabel *heighLabel;
@property (weak, nonatomic) IBOutlet UILabel *wightLabel;
@property (weak, nonatomic) IBOutlet UILabel *uniformNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *almaMaterLabel;

- (void)setData:(NSDictionary *)dicData;
- (NSDictionary *)getData;
- (NSURL *)getUrl;
- (void)setUrl:(NSURL *)str;
@end
