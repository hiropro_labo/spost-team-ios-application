//
//  MenuViewController.m
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2013/10/26.
//  Copyright (c) 2013年 北川 義隆. All rights reserved.
//

#import "TeamViewController.h"
#import "IPTeamModel.h"
#import "AFNetworking.h"
#import "TeamCustomCell.h"

#define MENU_CUSTOMCELL @"teamCustomCell"


@interface TeamViewController () {
    
    IBOutlet UIImageView *_imageView;
    IBOutlet UITableView *_tableView;
    
    IPTeamModel *_model;
}
@end


@implementation TeamViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {}
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    TRACE(@"");
    
    self.navigationItem.title = NAVIGATION_TITLE;
    
    // News Model の読み込み
    if ( ! _model || _model == nil) {
        _model = [[IPTeamModel alloc] init];
    }
    
    [_model download:YES block:nil];
    
    [self jsonLoad];
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    TRACE(@"");
    
    BOOL isCheck = YES;
    if (0 == [_model numberOfData]) {
        isCheck = NO;
    }
    
    [self menuImgDownload:isCheck];
    
    [_tableView registerNib:[UINib nibWithNibName:@"TeamCustomCell" bundle:nil] forCellReuseIdentifier:MENU_CUSTOMCELL];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Json data source

/**
 * NSUserDefaults に保存している JSON 情報を取得する
 */
- (void)jsonLoad
{
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = @"Loading...";
    
    [hud showAnimated:YES whileExecutingBlock:^{
        menuList = [_model loadList];
        menuArray = [[NSMutableArray alloc] init];
        
        for (NSDictionary *obj in menuList) {
            if (obj != nil)
            {
                [menuArray addObject:obj];
            }
        }
        
    } completionBlock:^{
        [hud removeFromSuperview];
        
        [_tableView reloadData];
    }];
}


#pragma mark - Table view data source

/**
 * セルの数
 */
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [menuArray count];
}


/**
 * セルの高さ
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 160;
}


/**
 * テーブルのセル表示処理
 */
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TeamCustomCell *cell = [_tableView dequeueReusableCellWithIdentifier:MENU_CUSTOMCELL forIndexPath:indexPath];
    
    // 画像ダウンロード
    NSString *strURL = [NSString stringWithFormat:@"%@%@",
                        URL_IMG_TEAM_MEMBER,
                        [[menuArray objectAtIndex:indexPath.row] objectForKey:JSON_KEY_TEAM_LIST_FILE]];
    NSURL *urlImage = [NSURL URLWithString:strURL];
    
    //画像がずれる症状のとりあえずの処置
    cell.contentView.frame = CGRectMake(0, 0, 0, 0);

    [cell.imageView setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:IMAGE_PLACEHOLDERIMAGE_MENU2]];

    return cell;
}


#pragma mark - Table view delegate

/**
 * セルの選択
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TRACE(@"TABLE CEL SELECT : %ld", (long)indexPath.row);
    
    [self performSegueWithIdentifier:@"teamDetail" sender:self];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}


#pragma mark - private method

/*
 * サーバからデータ取得するようモデルに要求
 */
- (void)menuImgDownload:(BOOL)check
{
    TRACE(@"");
    
    NSDictionary *dic = [_model loadTop];
    
    NSString *strURL;
    strURL = [NSString stringWithFormat:@"%@%@",
            URL_IMG_TEAM_TOP,
            [dic objectForKey:JSON_KEY_TEAM_TOP_FILE]];
    NSURL *urlImage = [NSURL URLWithString:strURL];

    TRACE(@"urlImage = %@", urlImage);

    [_imageView setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:IMAGE_PLACEHOLDERIMAGE_MENU1]];
    [_tableView reloadData];
}

- (NSDictionary *)getCurrData
{
    return [menuArray objectAtIndex:[_tableView indexPathForSelectedRow].row];
}

@end