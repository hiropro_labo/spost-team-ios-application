//
//  IPUtility.h
//  iPost
//
//  Created by 藤田　龍 on 2013/09/02.
//  Copyright (c) 2013年 hiropro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IPUtility : NSObject

+ (NSString *)dateFormatWithJsonTime:(NSString *)strTime format:(NSString *)format;
+ (void)telephoneCallWithStringNumber:(NSString *)strNumber;
+ (void)mailToWithStringAddress:(NSString *)address;
+ (UIStoryboard *)getMainStoryBoard;
+ (void)connectionAlertShow;
+ (void)convertAlertShow;
+ (void)mainteAlertShow;
+ (void)wait:(int)len;
@end
