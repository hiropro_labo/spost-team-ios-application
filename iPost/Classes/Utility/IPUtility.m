//
//  IPUtility.m
//  iPost
//
//  Created by 藤田　龍 on 2013/09/02.
//  Copyright (c) 2013年 hiropro. All rights reserved.
//

#import "IPUtility.h"

@implementation IPUtility

/*
 * JSONデータが秒数を格納しているので、日付の様式に変換
 */
+ (NSString *)dateFormatWithJsonTime:(NSString *)strTime format:(NSString *)format
{
    NSTimeInterval interval = [strTime doubleValue];
    NSDate *expiresDate = [NSDate dateWithTimeIntervalSince1970:interval];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    // [formatter setDateFormat:@"dd/MM/yyyy HH:mm"];
    [formatter setDateFormat:format];
    NSString *result = [formatter stringFromDate:expiresDate];
    return result;
}


+ (void)telephoneCallWithStringNumber:(NSString *)strNumber
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", strNumber]];
    [self openUrlWithUrl:url];
}


+ (void)mailToWithStringAddress:(NSString *)address
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"mailto:%@", address]];
    [self openUrlWithUrl:url];
}


+ (void)openUrlWithUrl:(NSURL *)url
{
    TRACE(@"url = %@", url);
    [[UIApplication sharedApplication] openURL:url];
}


+ (UIStoryboard *)getMainStoryBoard
{
    NSString *value = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"UIMainStoryboardFile"];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:value bundle:nil];
    
    return storyboard;
}


+ (void)connectionAlertShow
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:@"サーバーに接続できませんでした。\nインターネットに接続されているか確認して下さい。"
                                                   delegate:self
                                          cancelButtonTitle:@"閉じる"
                                          otherButtonTitles:nil, nil];
    [alert show];
}


+ (void)convertAlertShow
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:@"サーバーから情報が取得できておりません。\nインターネットに接続されているか確認の上、アプリケーションを起動し直して下さい。"
                                                   delegate:self
                                          cancelButtonTitle:@"閉じる"
                                          otherButtonTitles:nil, nil];
    [alert show];
}


+ (void)mainteAlertShow
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:@"現在、サーバーがメンテナンス中のため、サーバーから情報が取得できておりません。\n時間をおいてから再度、アプリケーションを起動し直して下さい。"
                                                   delegate:self
                                          cancelButtonTitle:@"閉じる"
                                          otherButtonTitles:nil, nil];
    [alert show];
}


/**
 * 指定したミリ秒待つようにする
 */
+ (void)wait:(int)len
{
    TRACE(@"WAIT...");
    
    int cnt = 0;
    // .3秒待つ
    while (cnt < len) {
        [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.0]];
        [NSThread sleepForTimeInterval:0.1];
        cnt ++;
    }
}

@end
