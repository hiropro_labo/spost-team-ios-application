//
//  ViewController.m
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2013/10/26.
//  Copyright (c) 2013年 北川 義隆. All rights reserved.
//

#import "ViewController.h"
#import "SlideViewController.h"


@interface ViewController ()
{
    IPMainModel     *_mainModel;
    IPNewsModel     *_newsModel;
    IPScheduleModel *_scheduleModel;
    IPResultModel   *_resultModel;
    IPTeamModel     *_teamModel;
    IPCheerModel     *_cheerModel;
    IPSponsorModel  *_sponsorModel;
    IPSettingModel  *_settingModel;
    IPMainMenuModel *_mainMenuModel;
}
@end


@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    defaults = [NSUserDefaults standardUserDefaults];
    [self setupModels];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    TRACE(@"");
    
    [self showUsingBlocks];
}

- (void)setupModels
{
    if (!_mainModel || _mainModel == nil) _mainModel = [[IPMainModel alloc] init];
    if (!_newsModel || _newsModel == nil) _newsModel = [[IPNewsModel alloc] init];
    if (!_scheduleModel || _scheduleModel == nil) _scheduleModel = [[IPScheduleModel alloc] init];
    if (!_resultModel || _resultModel == nil) _resultModel = [[IPResultModel alloc] init];
    if (!_teamModel || _teamModel == nil) _teamModel = [[IPTeamModel alloc] init];
    if (!_cheerModel || _cheerModel == nil) _cheerModel = [[IPCheerModel alloc] init];
    if (!_sponsorModel || _sponsorModel == nil) _sponsorModel = [[IPSponsorModel alloc] init];
    if (!_settingModel || _settingModel == nil) _settingModel = [[IPSettingModel alloc] init];
    if (!_mainMenuModel || _mainMenuModel == nil) _mainMenuModel = [[IPMainMenuModel alloc] init];
}

/**
 * JSONダウンロード
 */
- (void)showUsingBlocks
{
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = @"Loading...";
    
    [hud showAnimated:YES whileExecutingBlock:^{
        
        // メインJSONロード
        [_mainModel download:YES block:nil];
        
        // ニュースJSONロード
        [_newsModel download:YES block:nil];
        
        // スケジュールJSONロード
        [_scheduleModel download:YES block:nil];
        
        // リザルトJSONロード
        [_resultModel download:YES block:nil];
        
        // チームJSONロード
        [_teamModel download:YES block:nil];
        
        // cheerJSONロード
        [_cheerModel download:YES block:nil];
        
        // スポンサーJSONロード
        [_sponsorModel download:YES block:nil];
        
        // セッティングJSONロード
        [_settingModel download:YES block:nil];
        
        // メニューJSONロード
        [_mainMenuModel download:YES block:nil];
        
    } completionBlock:^{
        [hud removeFromSuperview];
        [self locationMain];
    }];
}

/**
 * View to Mainte
 */
- (void)locationMain
{
    SlideViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SlideView"];
    [self presentViewController:controller animated:NO completion:nil];
}

/*
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    TRACE(@"");

    @try {
        NSDictionary *mainteJson;
        mainteJson = [_mainteModel load];
        
        // メンテナンス状況確認
        if (mainteJson != nil) {
            
            NSString *mainteStatus   = [mainteJson objectForKey:JSON_KEY_MAINTENANCE_VALUE];
            NSDictionary *shopStatus = [mainteJson objectForKey:JSON_KEY_MAINTENANCE_SHOPSTATUS];
            TRACE(@"MAINTENANCE STATUS : %@", mainteStatus);
            
            NSString *status = [shopStatus objectForKey:JSON_KEY_MAINTE_STATUS_CODE];
            TRACE(@"MAINTENANCE CODE : %@", status);
            
            if( ! [mainteStatus isEqual:[NSNull null]] && mainteStatus != nil)
            {
                TRACE(@"MAINTENANCE NOW !");
                [IPUtility mainteAlertShow];
            }
            else
            {
                TRACE(@"NO MAINTENANCE ! ");
                [self showUsingBlocks];
            }
        } else {
            TRACE(@"NO MAINTENANCE JSON DATE ! ");
            [IPUtility convertAlertShow];
        }
    }
    @catch (NSException *exception) {
        TRACE(@"%@", exception);
    }
}
*/
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
