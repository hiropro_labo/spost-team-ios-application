//
//  WebViewController.m
//  iPost-Mock3
//
//  Created by 北川 義隆 on 2014/01/05.
//  Copyright (c) 2014年 北川 義隆. All rights reserved.
//

#import "WebViewController.h"
#import "IPMainModel.h"


@interface WebViewController () {
    IBOutlet UIBarButtonItem *_buttonBack;
    IBOutlet UIBarButtonItem *_buttonForward;
    IBOutlet UIBarButtonItem *_buttonRefresh;
    IBOutlet UIBarButtonItem *_buttonShare;
    IBOutlet UIBarButtonItem *_buttonClose;
    
    IBOutlet UIView *_titleView;
    IBOutlet UILabel *_lblTitle;
    
    NJKWebViewProgressView *_progressView;
    NJKWebViewProgress *_progressProxy;
    
    NSURL *_currURL;
}
@end

@implementation WebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    TRACE(@"");
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    TRACE(@"");
    
    _progressProxy = [[NJKWebViewProgress alloc] init];
    _webView.delegate = _progressProxy;
    _progressProxy.webViewProxyDelegate = self;
    _progressProxy.progressDelegate = self;
    
    CGSize titleSize = _titleView.frame.size;
    CGFloat progressBarHeight = 2.5f;
    CGRect barFrame = CGRectMake(0., titleSize.height - progressBarHeight, titleSize.width, progressBarHeight);
    _progressView = [[NJKWebViewProgressView alloc] initWithFrame:barFrame];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    TRACE(@"");
    
    [self loadPageByServerData];
    [_titleView addSubview:_progressView];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    TRACE(@"");

    self.webView.delegate = nil;

    [_progressView removeFromSuperview];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - delegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    TRACE(@"");
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self updateButton];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    TRACE(@"");
    [self updateButton];
    _buttonShare.enabled = TRUE;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self updateButton];
}

#pragma mark - action

- (IBAction)actClose:(id)sender
{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];

    TRACE(@"");
    if ([[UIWebView appearance] isLoading]) {
        TRACE(@"stopLoading");
        [[UIWebView appearance] stopLoading];
    }
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actShare:(id)sender
{
    UIActionSheet *as = [[UIActionSheet alloc] init];
    as.delegate = self;
    [as addButtonWithTitle:@"Safariで開く"];
    [as addButtonWithTitle:@"コピー"];
    [as addButtonWithTitle:@"キャンセル"];
    as.cancelButtonIndex = 2;
    [as showInView:self.view];
}

#pragma mark - public method

- (void)setURL:(NSURL *)url
{
    TRACE(@"url = %@", url);
    _currURL = url;
    TRACE(@"_currURL = %@", _currURL);
}

- (void)requestPage
{
    TRACE(@"");
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:_currURL
                                  cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                              timeoutInterval:30];
    
    //NSURLRequest *req = [NSURLRequest requestWithURL:_currURL];
    [[UIWebView appearance] loadRequest:urlRequest];
}

#pragma mark - private method

- (void)loadPageByServerData
{
    [self requestPage];
    [self updateButton];
}

- (void)updateButton
{
    TRACE(@"");
    if ([_webView canGoBack]) {
        _buttonBack.enabled = YES;
    } else {
        _buttonBack.enabled = NO;
    }
    
    if ([_webView canGoForward]) {
        _buttonForward.enabled = YES;
    } else {
        _buttonForward.enabled = NO;
    }
}

#pragma mark - ActionSheet Delegate
-(void)actionSheet:(UIActionSheet*)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSURLRequest* request = _webView.request;
    NSURL* url = [request URL];
    NSString* urlstr = [url absoluteString];
    
    TRACE(@"%@", urlstr);
    
    switch (buttonIndex) {
        case 0:{ // Safariで開く
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", urlstr]]];
        }
            break;
            
        case 1:{ // コピー
            UIPasteboard *board = [UIPasteboard generalPasteboard];
            [board setValue:urlstr forPasteboardType:@"public.utf8-plain-text"];
        }
            break;
    }
}


#pragma mark - NJKWebViewProgress Delegate
-(void)webViewProgress:(NJKWebViewProgress *)webViewProgress updateProgress:(float)progress
{
    [_progressView setProgress:progress animated:YES];
    _lblTitle.text = [_webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    
    [self updateButton];
}

@end
